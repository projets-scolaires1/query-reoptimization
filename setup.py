"""Setup the test environment.

This Python script is used to setup the test environment with a specific PostgreSQL algorithm.

Setting up the environment involves the following steps:
- Checking script parameters ;
- Installation of prerequisites (bash script `libs/scripts/prerequisites.sh`) ;
- Installation of the chosen PostgreSQL version;
- Generate data with the TPC-H Benchmark tool (if not already available);
- Create database and populate with generated data.

Usage:
    setup.py [-o <output>] [-p <pg_install_path>] [-d <disk>] [-a <algorithm>] [-s <scale>] [-q <nb_queries>] [-v]

Arguments:
    -o <output>, --output <output>
        The path to the output directory.
        Default: ./output

    -p <pg_install_path>, --pg-install-path <pg_install_path>
        The path to the PostgreSQL installation directory.
        Default: /usr/local/pgsql

    -d <disk>, --disk <disk>
        The disk to use (e.g. disk1).
        Default: disk1

    -a <algorithm>, --algorithm <algorithm>
        The algorithm to use.
        Default: no_reoptimization

    -s <scale>, --scale <scale>
        The scale factor to use.
        Default: 1

    -q <nb_queries>, --nb-queries <nb_queries>
        The number of queries to generate (e.g. 22 queries is one query per type).
        Default: 22

    -v, --verbose
        Increase output verbosity.

Example:
    python3 setup.py -o ./output -a querysplit -s 0.1 -q 22

Output:
    PostgreSQL installation will create the folder provided with the `pg_install_path` argument and install the files required for its operation.
    For TPC-H data generation, this works in the same way as for the `generate_data.py` script.
"""

import logging
import argparse
import os
import shutil
import subprocess

from libs.tpch import TPCH, CreateDatabase


TPCH_CONF = {
    "path": "./tpch-tool",
    "compiler": "gcc",
    "database": "POSTGRESQL",
    "machine": "LINUX",
    "workload": "TPCH"
}


def run_cmd(cmd: str) -> None:
    """Run a command line.

    Args:
        cmd (str): The command to run.
    """
    logging.debug(f"SETUP: Run command: {cmd}")

    try:
        cmd_array = cmd.split(" ")
        subprocess.run(cmd_array, check=True)
    except subprocess.CalledProcessError as e:
        logging.error(f"SETUP: Command failed: {cmd} {e}")
        raise Exception(f"SETUP: Command failed: {cmd} {e}")


# Use parse_args() to get the arguments
parser = argparse.ArgumentParser(
    prog="setup.py",
    description="Execute TPC-H Benchmark queries on a specific PostgreSQL algorithm.")

parser.add_argument(
    "-o", "--output", type=str, default="./output",
    help="The path to the output directory. Defaults to ./output.")
parser.add_argument(
    "-p", "--pg-install-path", type=str, default="/usr/local/pgsql",
    help="The path to the PostgreSQL installation directory. Defaults to /usr/local/pgsql.")
parser.add_argument(
    "-d", "--disk", type=str, default="disk1",
    help="The disk to use (e.g. disk1). Defaults to disk1.")
parser.add_argument(
    "-a", "--algorithm", type=str, default="no_reoptimization",
    help="The algorithm to use. Defaults to no_reoptimization.")
parser.add_argument(
    "-s", "--scale", type=float, default=1,
    help="The scale factor to use. Defaults to 1.")
parser.add_argument(
    "-q", "--nb-queries", type=int, default=22,
    help="The number of queries to generate (e.g. 22 queries is one query per type). Defaults to 22.")
parser.add_argument(
    "-v", "--verbose", action="store_true",
    help="Increase output verbosity.")

args = parser.parse_args()

# Configure the logger
level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(
    level=level,
    format="%(asctime)s :: %(levelname)s :: %(message)s")

# Variables
queries_path = f"{args.output}/queries_{args.nb_queries}"
data_path = f"{args.output}/data_{args.scale}"

# 0. Check parameters
logging.info("SETUP: 0. Check parameters")
if not os.path.exists(f"algorithms/{args.algorithm}"):
    logging.error(f"SETUP: The algorithm {args.algorithm} does not exist.")
    raise Exception(f"SETUP: The algorithm {args.algorithm} does not exist.")
if args.nb_queries % 22 != 0:
    logging.error(
        "SETUP: The number of queries must be a multiple of 22 (at least 22 for one query per type).")
    raise Exception(
        "SETUP: The number of queries must be a multiple of 22 (at least 22 for one query per type).")

# 1. Move existing queries if exists
logging.info("SETUP: 1. Move existing queries if exists")
if os.path.exists(queries_path):
    shutil.move(queries_path, os.path.expanduser(
        f"~/queries_{args.nb_queries}"))

# 2. Install prerequisites
logging.info("SETUP: 2. Install prerequisites")
run_cmd(
    f"bash libs/scripts/prerequisites.sh {args.disk} {args.pg_install_path}")

# 3. Create the output directory if it does not exist and add existing queries if exist
logging.info(
    "SETUP: 3. Create the output directory if it does not exist and add existing queries if exist")
os.makedirs(args.output, exist_ok=True)
if os.path.exists(queries_path):
    shutil.move(os.path.expanduser(
        f"~/queries_{args.nb_queries}"), queries_path)

# 4. Install the PostgreSQL instance
logging.info("SETUP: 4. Install the PostgreSQL instance")
run_cmd(
    f"bash libs/scripts/install_postgres.sh algorithms/{args.algorithm}/postgresql-12.3 {args.pg_install_path}")
run_cmd(f"python3 algorithms/{args.algorithm}/initiate.py")

# 5. Generate the data if it does not exist
logging.info("SETUP: 5. Generate the data (if it does not exist)")
if not os.path.exists(data_path) and not os.path.exists(queries_path):
    tpch = TPCH(
        TPCH_CONF["path"], TPCH_CONF["compiler"], TPCH_CONF["database"],
        TPCH_CONF["machine"], TPCH_CONF["workload"])

    if not os.path.exists(data_path):
        tpch.generate_database(args.scale, data_path)
    if not os.path.exists(queries_path):
        tpch.generate_queries(args.nb_queries, queries_path)

# 6. Create the database and populate it with the data
logging.info("SETUP: 6. Create the database and populate it with the data")
psql_db = CreateDatabase()
psql_db.create_database()
psql_db.create_tables()
psql_db.populate_tables(data_path=data_path)
psql_db.create_indexes()

logging.info("SETUP: Done")
