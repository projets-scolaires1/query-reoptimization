"""Initiate QuerySplit.

Description:
    This module execute the following commands in PostgreSQL:

    > switch to relationshipcenter;
    > switch to c_r;

    The commands generate errors but they are ignored.

Usage:
    initiate.py [-d <db_name>] [-u <db_user>] [-p <db_password>] [-H <db_host>] [-P <db_port>] [-v]

Arguments:
    -d <db_name>, --db-name <db_name>
        Database name.
        Default: tpch

    -u <db_user>, --db-user <db_user>
        Database user.
        Default: postgres

    -p <db_password>, --db-password <db_password>
        Database password.
        Default: postgres

    -H <db_host>, --db-host <db_host>
        Database host.
        Default: localhost

    -P <db_port>, --db-port <db_port>
        Database port.
        Default: 5432

    -v, --verbose
        Increase output verbosity.

Example:
    python3 algorithms/querysplit/initiate.py
"""

import logging
import argparse
import psycopg2


def initiate_querysplit(db_name: str, db_user: str, db_password: str, db_host: str, db_port: str) -> None:
    """Initiate QuerySplit.

    Args:
        db_name (str): Database name.
        db_user (str): Database user.
        db_password (str): Database password.
        db_host (str): Database host.
        db_port (str): Database port.
    """
    # Connect to the database
    conn = psycopg2.connect(
        f"dbname={db_name} user={db_user} password={db_password} host={db_host} port={db_port}")
    conn.autocommit = True
    cursor = conn.cursor()

    try:
        cursor.execute("SWITCH TO relationshipcenter;")
    except (Exception, psycopg2.DatabaseError) as error:
        logging.warning(error)

    try:
        cursor.execute("SWITCH TO c_r;")
    except (Exception, psycopg2.DatabaseError) as error:
        logging.warning(error)

    if cursor is not None:
        cursor.close()
    if conn is not None:
        conn.close()


# Use parse_args() to get the arguments
parser = argparse.ArgumentParser(
    prog="initiate.py",
    description="Initiate QuerySplit.")

parser.add_argument(
    "-d", "--db-name", type=str,
    help="Database name.",
    default="template1")
parser.add_argument(
    "-u", "--db-user", type=str,
    help="Database user.",
    default="postgres")
parser.add_argument(
    "-p", "--db-password", type=str,
    help="Database password.",
    default="postgres")
parser.add_argument(
    "-H", "--db-host", type=str,
    help="Database host.",
    default="localhost")
parser.add_argument(
    "-P", "--db-port", type=str,
    help="Database port.",
    default="5432")
parser.add_argument(
    "-v", "--verbose", action="store_true",
    help="Increase output verbosity.")

args = parser.parse_args()

# Configure the logger
level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(
    level=level,
    format="%(asctime)s :: %(levelname)s :: %(message)s")

# Initiate QuerySplit
initiate_querysplit(
    args.db_name, args.db_user, args.db_password,
    args.db_host, args.db_port)

logging.info("QuerySplit initiated.")
