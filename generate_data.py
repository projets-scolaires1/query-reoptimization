"""Generate TPC-H Benchmark data (database and queries).

This Python script is used to generate database data (in CSV format) or a set of queries (SQL files) with TPC-H Benchmark tool.

Usage:
    generate_data.py [-p <path>] [-o <output>] [-c <compiler>] [-d <database>] [-m <machine>] [-w <workload>] [-s <scale>] [-q <nb_queries>] [-f] [-v]

Arguments:
    -p <path>, --path <path>
        The path to the TPC-H Benchmark tool.
        Default: ./tpch-tool

    -o <output>, --output <output>
        The path to the output directory.
        Default: ./output

    -c <compiler>, --compiler <compiler>
        The compiler to use.
        Default: gcc

    -d <database>, --database <database>
        The database to use.
        Default: POSTGRESQL

    -m <machine>, --machine <machine>
        The machine to use.
        Default: LINUX

    -w <workload>, --workload <workload>
        The workload to use.
        Default: TPCH

    -s <scale>, --scale <scale>
        The scale factor to use.

    -q <nb_queries>, --nb-queries <nb_queries>
        The number of queries to generate (e.g. 22 queries is one query per type).

    -f, --force
        Force the generation of the data even if the output directories already exist.

    -v, --verbose
        Increase output verbosity.

Example:
    python3 generate_data.py -p ./tpch-tool -o ./output -s 1 -q 22

Output:
    Results depend on the options chosen. The results are located in the folder specified by the `--output` argument.

    For queries, the format is: `<output>/queries_<nb_queries>`:

    ```bash
    output/queries_22
    └── execution_1
        ├── 10-12.sql
        ├── 11-18.sql
        ├── ...
        └── 9-9.sql
    ```

    The command divides the number of queries by 22 and creates a folder of 22 queries for each group, in `execution_<i>` format. These folders then contain one SQL file per query type, in the format `<query_type>-<execution_order>.sql`.

    For data, the format is: `<output>/data_<scale>`:

    ```bash
    output/data_0.1
    ├── customer.csv
    ├── lineitem.csv
    ├── nation.csv
    ├── orders.csv
    ├── part.csv
    ├── partsupp.csv
    ├── region.csv
    └── supplier.csv
    ```

    The command generates a CSV file for each table in the database.
"""

import logging
import argparse
import os

from libs.tpch.tpch import TPCH


# Use parse_args() to get the arguments
parser = argparse.ArgumentParser(
    prog="generate_data.py",
    description="Generate TPC-H Benchmark data (database and queries).")

parser.add_argument(
    "-p", "--path", type=str, default="./tpch-tool",
    help="The path to the TPC-H Benchmark tool. Defaults to ./tpch-tool.")
parser.add_argument(
    "-o", "--output", type=str, default="./output",
    help="The path to the output directory. Defaults to ./output.")
parser.add_argument(
    "-c", "--compiler", type=str, default="gcc",
    help="The compiler to use. Defaults to gcc.")
parser.add_argument(
    "-d", "--database", type=str, default="POSTGRESQL",
    help="The database to use. Defaults to POSTGRESQL.")
parser.add_argument(
    "-m", "--machine", type=str, default="LINUX",
    help="The machine to use. Defaults to LINUX.")
parser.add_argument(
    "-w", "--workload", type=str, default="TPCH",
    help="The workload to use. Defaults to TPCH.")
parser.add_argument(
    "-s", "--scale", type=float,
    help="The scale factor to use.")
parser.add_argument(
    "-q", "--nb-queries", type=int,
    help="The number of queries to generate (e.g. 22 queries is one query per type).")
parser.add_argument(
    "-f", "--force", action="store_true",
    help="Force the generation of the data even if the output directories already exist.")
parser.add_argument(
    "-v", "--verbose", action="store_true",
    help="Increase output verbosity.")

args = parser.parse_args()

# Configure the logger
level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(
    level=level,
    format="%(asctime)s :: %(levelname)s :: %(message)s")

# Variables
data_path = f"{args.output}/data_{args.scale}"
queries_path = f"{args.output}/queries_{args.nb_queries}"

tpch = TPCH(
    args.path, args.compiler, args.database,
    args.machine, args.workload)

# Generate the data if parameters is specified
if (args.scale and not os.path.exists(data_path)) or (args.scale and args.force):
    logging.info("GENERATE_DATA: Generate TPC-H database data")
    tpch.generate_database(args.scale, data_path)
if (args.nb_queries and not os.path.exists(queries_path)) or (args.nb_queries and args.force):
    logging.info("GENERATE_DATA: Generate TPC-H queries")
    tpch.generate_queries(args.nb_queries, queries_path)

logging.info("GENERATE_DATA: Done")
