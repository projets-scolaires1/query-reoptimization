"""Run a complete test on the Grid'5000 platform.

This is a Python script for running a complete test on the Grid'5000 platform, which involves the following steps:

- Reserve a Grid'5000 node (see Grid'5000 documentation)
- Execute a bash script to:
    - Retrieve the project;
    - Retrieve any data sets;
    - Configure the environment ([setup.py](http://setup.py/) script);
    - Run the queries ([run.py](http://run.py/) script);
    - And retrieve the results in the *results/* folder on the Grid'5000 server.

Usage:
    g5k_execution.py -u <g5k_user> -p <g5k_pkey_path> -n <node> -s <site> [-d <disk>] [-w <walltime>] [-o <node_pg_data_path>] [-a <algorithm>] [-S <scale>] [-q <nb_queries>] [-t <nb_trials>] [-T <tests_type>] [-c <nb_connections>] [-U <git_project_url>] [-b <git_project_branch>] [-v]

Arguments:
    -u <g5k_user>, --g5k-user <g5k_user>
        The Grid'5000 username.

    -p <g5k_pkey_path>, --g5k-pkey-path <g5k_pkey_path>
        The path to the Grid'5000 private key.
        Default: ~/.ssh/grid5000_key

    -n <node>, --node <node>
        The node to reserve (e.g. chifflot-1).

    -s <site>, --site <site>
        The site to use (e.g. lille).

    -d <disk>, --disk <disk>
        The disk to use (e.g. disk1).
        Default: disk1

    -w <walltime>, --walltime <walltime>
        The walltime for the node reservation.
        Default: 00:30

    -o <node_pg_data_path>, --node-pg-data-path <node_pg_data_path>
        The path to the PostgreSQL data directory on the node.
        Default: /usr/local/pgsql

    -a <algorithm>, --algorithm <algorithm>
        The algorithm to use.
        Default: no_reoptimization

    -S <scale>, --scale <scale>
        The scale factor to use.
        Default: 1

    -q <nb_queries>, --nb-queries <nb_queries>
        The number of queries to generate (e.g. 22 queries is one query per type).
        Default: 22

    -t <nb_trials>, --nb-trials <nb_trials>
        Number of trials.
        Default: 1

    -T <tests_type>, --tests-type <tests_type>
        The type of tests (queries execution) to run (e.g. serial or parallel).
        Default: serial

    -c <nb_connections>, --nb-connections <nb_connections>
        The number of connections to use for parallel execution tests.
        Default: 2

    -U <git_project_url>, --git-project-url <git_project_url>
        The Git project URL containing test programs and algorithms.
        Default: https://gitlab.papierpain.fr/enssat/query-reoptimization-project

    -b <git_project_branch>, --git-project-branch <git_project_branch>
        The Git project branch to use.
        Default: master

    -v, --verbose
        Increase output verbosity.

Example:
    python3 g5k_execution.py -u sgourves -p ~/.ssh/grid5000_key -n chifflot-1 -s lille -a querysplit -S 10 -q 220 -t 5

Output:
    1. Reservation failed
        
        If the reservation fails, check your settings and if they are correct, this means that the Grid'5000 node is already reserved by someone else on the platform.
        See the current reservations on this link (adapt the site name by your own with a capital letter on the first letter): https://intranet.grid5000.fr/oar/Lille/drawgantt-svg
        
    2. Reservation ended successfully
        
        When your reservation is ended correctly, it means that on the reservations link, your node shows “terminated” state. If it’s the case, your results are on the server (in the chosen site, e.g. `ssh lille.g5k`) in the *results/* folder.
        
        The results are structured as follow:
        
        ```bash
        results
        └── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
            ├── execution_times.json # Contains the execution time of each trials (time between the start and the end of the trial)
            ├── trial_1 # Contains results of the first trial
            │   └── output_execution_1.csv # Contains execution time and monetary cost of each query of the query set
            ├── trial_2
            └── trial_3
        ```
        
        The results folder format is the following:
        
        ```bash
        results/<algorithm>_<tests_type>_<nb_connections>_<node_hostname>_s<scale>_q<nb_queries>_t<nb_trials>
        ```
        
    3. Reservation ended in failure
        
        When it fails, it means that the time is up (insufficient **walltime**) or that the test has failed.
        
        To check the status and this logs, you can connect you on the Grid’5000 site with:
        
        ```bash
        ssh lille.g5k
        oarstat # To check the status of all reserved nodes (for the site)
        cat OAR.1962412.stderr # To check the error output of the test (the number 1962412 is the job id)
        cat OAR.1962412.stdout # To check the output of the test (the number 1962412 is the job id)
        ```
"""

import logging
import argparse

from libs.g5k import G5kNode


# Use parse_args() to get the arguments
parser = argparse.ArgumentParser(
    prog="g5k_execution.py",
    description="Execute TPC-H Benchmark queries on a specific PostgreSQL algorithm on a Grid'5000 node.")

parser.add_argument(
    "-u", "--g5k-user", type=str, required=True,
    help="The Grid'5000 username.")
parser.add_argument(
    "-p", "--g5k-pkey-path", type=str, default="~/.ssh/grid5000_key",
    help="The path to the Grid'5000 private key. Defaults to ~/.ssh/grid5000_key.")
parser.add_argument(
    "-n", "--node", type=str, required=True,
    help="The node to reserve (e.g. chifflot-1).")
parser.add_argument(
    "-s", "--site", type=str, required=True,
    help="The site to use (e.g. lille).")
parser.add_argument(
    "-d", "--disk", type=str, default="disk1",
    help="The disk to use (e.g. disk1). Defaults to disk1.")
parser.add_argument(
    "-w", "--walltime", type=str, default="00:30",
    help="The walltime for the node reservation. Defaults to 00:30.")
parser.add_argument(
    "-o", "--node-pg-data-path", type=str, default="/usr/local/pgsql",
    help="The path to the PostgreSQL data directory on the node. Defaults to /usr/local/pgsql.")
parser.add_argument(
    "-a", "--algorithm", type=str, default="no_reoptimization",
    help="The algorithm to use. Defaults to no_reoptimization.")
parser.add_argument(
    "-S", "--scale", type=float, default=1,
    help="The scale factor to use. Defaults to 1.")
parser.add_argument(
    "-q", "--nb-queries", type=int, default=22,
    help="The number of queries to generate (e.g. 22 queries is one query per type). Defaults to 22.")
parser.add_argument(
    "-t", "--nb-trials", type=int, default=1,
    help="Number of trials. Defaults to 1.")
parser.add_argument(
    "-T", "--tests-type", type=str, default="serial",
    help="The type of tests (queries execution) to run (e.g. serial or parallel).")
parser.add_argument(
    "-c", "--nb-connections", type=int, default=2,
    help="The number of connections to use for parallel execution tests. Defaults to 2.")
parser.add_argument(
    "-U", "--git-project-url", type=str, default="https://gitlab.papierpain.fr/enssat/query-reoptimization-project",
    help="The Git project URL containing test programs and algorithms. Defaults to https://gitlab.papierpain.fr/enssat/query-reoptimization-project.")
parser.add_argument(
    "-b", "--git-project-branch", type=str, default="master",
    help="The Git project branch to use. Defaults to master.")
parser.add_argument(
    "-v", "--verbose", action="store_true",
    help="Increase output verbosity.")

args = parser.parse_args()

# Configure the logger
level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(
    # filename=f"{args.node}_execution.log", # For debugging only
    level=level,
    format="%(asctime)s :: %(levelname)s :: %(message)s")

# Variables
workspace_path = f"~/{args.node}/reoptimization"
# Path to the existing queries if already exist
tpch_queries_path = f"~/generated_queries/queries_{args.nb_queries}"
script_to_execute = f"sudo-g5k bash ./g5k_init.sh {args.git_project_branch} {args.git_project_url} {workspace_path} {args.algorithm} {args.node_pg_data_path}/output {args.scale} {args.nb_queries} {args.nb_trials} {args.site} {args.disk} {args.node_pg_data_path} {tpch_queries_path} {args.tests_type} {args.nb_connections}"

logging.info("G5K_EXECUTION: Reserve the Grid'5000 node")
g5k = G5kNode(
    g5k_user=args.g5k_user, g5k_pkey=args.g5k_pkey_path,
    node=args.node, site=args.site, disk=args.disk)
g5k.reserve(walltime=args.walltime, script=script_to_execute)

logging.info("G5K_EXECUTION: Done")
