"""Execute TPC-H Benchmark queries on a specific PostgreSQL algorithm.

This Python script is used to execute TPC-H Benchmark queries on a specific PostgreSQL algorithm.

Usage:
    run.py [-o <output>] [-a <algorithm>] [-s <scale>] [-q <nb_queries>] [-t <nb_trials>] [-T <tests_type>] [-c <nb_connections>] [-v]

Arguments:
    -o <output>, --output <output>
        The path to the output directory.
        Default: ./output

    -a <algorithm>, --algorithm <algorithm>
        The algorithm to use.
        Default: no_reoptimization

    -s <scale>, --scale <scale>
        The scale factor to use.
        Default: 1

    -q <nb_queries>, --nb-queries <nb_queries>
        The number of queries to generate (e.g. 22 queries is one query per type).
        Default: 22

    -t <nb_trials>, --nb-trials <nb_trials>
        Number of trials.
        Default: 1

    -T <tests_type>, --tests-type <tests_type>
        The type of tests (queries execution) to run (e.g. serial or parallel).
        Default: serial

    -c <nb_connections>, --nb-connections <nb_connections>
        The number of connections to use for parallel execution tests.
        Default: 2

    -v, --verbose
        Increase output verbosity.

Example:
    python3 run.py -o ./output -a querysplit -s 0.1 -q 22 -t 3

Output:
    The results are structured as follow:

    ```bash
    <output>/results
    └── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
        ├── execution_times.json # Contains the execution time of each trials (time between the start and the end of the trial)
        ├── trial_1 # Contains results of the first trial
        │   └── output_execution_1.csv # Contains execution time and monetary cost of each query of the query set
        ├── trial_2
        └── trial_3
    ```

    The results folder format is the following:

    ```bash
    <output>/results/<algorithm>_<tests_type>_<nb_connections>_<hostname>_s<scale>_q<nb_queries>_t<nb_trials>
    ```
"""

import logging
import argparse
import os

from libs.tpch import RunQueries


# Use parse_args() to get the arguments
parser = argparse.ArgumentParser(
    prog="run.py",
    description="Execute TPC-H Benchmark queries on a specific PostgreSQL algorithm.")

parser.add_argument(
    "-o", "--output", type=str, default="./output",
    help="The path to the output directory. Defaults to ./output.")
parser.add_argument(
    "-a", "--algorithm", type=str, default="no_reoptimization",
    help="The algorithm to use. Defaults to no_reoptimization.")
parser.add_argument(
    "-s", "--scale", type=float, default=1,
    help="The scale factor to use. Defaults to 1.")
parser.add_argument(
    "-q", "--nb-queries", type=int, default=22,
    help="The number of queries to generate (e.g. 22 queries is one query per type). Defaults to 22.")
parser.add_argument(
    "-t", "--nb-trials", type=int, default=1,
    help="Number of trials. Defaults to 1.")
parser.add_argument(
    "-T", "--tests-type", type=str, default="serial",
    help="The type of tests (queries execution) to run (e.g. serial or parallel). Defaults to serial.")
parser.add_argument(
    "-c", "--nb-connections", type=int, default=2,
    help="The number of connections to use for parallel execution tests. Defaults to 2.")
parser.add_argument(
    "-v", "--verbose", action="store_true",
    help="Increase output verbosity.")

args = parser.parse_args()

# Configure the logger
level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(
    level=level,
    format="%(asctime)s :: %(levelname)s :: %(message)s")

# Variables
queries_path = f"{args.output}/queries_{args.nb_queries}"
hostname = os.popen("hostname").read().strip()
# If hostname contains a dot, remove the domain name (e.g. "node-1.grid5000.fr" becomes "node-1")
if "." in hostname:
    hostname = hostname.split(".")[0]
results_path = f"{args.output}/results/{args.algorithm}_{args.tests_type}_{args.nb_connections}_{hostname}_s{args.scale}_q{args.nb_queries}_t{args.nb_trials}"

# 1. Execute the queries
logging.info("RUN: 1. Execute the queries")
run_test = RunQueries()
# run_test.run(queries_path, args.nb_trials, results_path)
if args.tests_type == "serial":
    run_test.run_serial(queries_path, results_path, args.nb_trials)
elif args.tests_type == "parallel":
    run_test.run_parallel(
        queries_path, results_path, args.nb_connections, args.nb_trials)
else:
    logging.error("Unknown tests type.")
    raise Exception("Unknown tests type.")

logging.info("RUN: Done")
