#!/bin/bash
set -e

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#! MAIN:
#!   Install the PostgreSQL database from a postgresql path on a Grid'5000 node.
#!
#! AUTHORS:
#!   Steven Gourves
#!
#! USAGE:
#!   ./install_postgres.sh <postgresql_path> <pg_install_path>
#!
#! PARAMETERS:
#!   <postgresql_path>: The path to the PostgreSQL source code (in the 'algorithms' directory).
#!   <pg_install_path>: The path to the PostgreSQL data and configuration (after installation).
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

check_parameters() {
    # Parameters
    POSTGRESQL_PATH="$1"

    # Check if the postgresql path is set
    if [ -z "${POSTGRESQL_PATH}" ]; then
        echo "POSTGRESQL_PATH is not set"
        exit 1
    fi

    # Check if the postgresql path exists
    if [ ! -d "${POSTGRESQL_PATH}" ]; then
        echo "POSTGRESQL_PATH does not exist"
        exit 1
    fi

    # Check if this file is executed with root privileges
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be executed with root privileges"
        exit 1
    fi
}

kill_postgres_process() {
    # if there are any running processes, stop them
    # We use:
    # $ lsof -i -P -n | grep LISTEN | grep 5432 | awk '{print $2}'
    #
    # And then kill the processes with:
    # $ kill -9 <pid>

    PID=$(lsof -i -P -n | grep LISTEN | grep 5432 | awk '{print $2}')

    if [ ! -z "${PID}" ]; then
        kill -9 ${PID}
        sleep 10 # To be sure that the process is killed
    fi
}

edit_param_in_file() {
    # - Check if the parameter exists
    # - If it exists, replace by the line '<parameter> = <value>'
    # - If it does not exist, add the line '<parameter> = <value>'

    # Parameters
    FILE="$1"
    PARAMETER="$2"
    VALUE="$3"

    # Replace the pattern by the replacement in the file
    if grep -q "^[# ]*${PARAMETER}[ ]*=.*" "${FILE}"; then
        sed -i "s/^[# ]*${PARAMETER}[ ]*=.*/${PARAMETER} = ${VALUE}/" "${FILE}"
    else
        echo "${PARAMETER} = ${VALUE}" >>"${FILE}"
    fi
}

configure_postgres() {
    # Parameters
    PG_INSTALL_PATH="$1"

    # Variables
    POSTGRESQL_CONF="${PG_INSTALL_PATH}/data/postgresql.conf"
    SHARED_BUFFERS="128GB" # The value is 40% of the RAM
    TEMP_BUFFERS="64GB"    # The value is shared_buffers / 2
    WORK_MEM="16GB"        # The value is temp_buffers / 4

    # Check environment specifications
    # If RAM < 128GB, then we use the recommended values (cf. https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server)
    RAM=$(free -g | grep Mem | awk '{print $2}')
    if [ "${RAM}" -lt "190" ]; then
        SHARED_BUFFERS="$((${RAM} * 6 / 10))GB"
        TEMP_BUFFERS="$((${RAM} / 5))GB"
        WORK_MEM="$((${RAM} / 20))GB"

        # If WORK_MEM is 0, change it in MB
        if [ "${WORK_MEM}" == "0GB" ]; then
            MB_RAM=$((${RAM} * 1024))
            WORK_MEM="$((${MB_RAM} / 20))MB"
        fi
    fi

    edit_param_in_file "${POSTGRESQL_CONF}" "shared_buffers" "${SHARED_BUFFERS}"
    edit_param_in_file "${POSTGRESQL_CONF}" "temp_buffers" "${TEMP_BUFFERS}"
    edit_param_in_file "${POSTGRESQL_CONF}" "work_mem" "${WORK_MEM}"

    edit_param_in_file "${POSTGRESQL_CONF}" "dynamic_shared_memory_type" "posix"
    edit_param_in_file "${POSTGRESQL_CONF}" "bgwriter_delay" "10ms"
    edit_param_in_file "${POSTGRESQL_CONF}" "bgwriter_lru_maxpages" "100"
    edit_param_in_file "${POSTGRESQL_CONF}" "bgwriter_lru_multiplier" "1.0"
    edit_param_in_file "${POSTGRESQL_CONF}" "bgwriter_flush_after" "100"
    edit_param_in_file "${POSTGRESQL_CONF}" "effective_io_concurrency" "0"
    edit_param_in_file "${POSTGRESQL_CONF}" "checkpoint_timeout" "30s"
    edit_param_in_file "${POSTGRESQL_CONF}" "max_wal_size" "1GB"
    edit_param_in_file "${POSTGRESQL_CONF}" "min_wal_size" "80MB"
    edit_param_in_file "${POSTGRESQL_CONF}" "enable_partitionwise_join" "on"
    edit_param_in_file "${POSTGRESQL_CONF}" "enable_partitionwise_aggregate" "on"
    edit_param_in_file "${POSTGRESQL_CONF}" "geqo" "on"
    edit_param_in_file "${POSTGRESQL_CONF}" "track_io_timing" "on"
}

install_postgres() {
    # Parameters
    POSTGRESQL_PATH="$1"
    PG_INSTALL_PATH="$2"

    check_parameters "${POSTGRESQL_PATH}"

    # Variables
    CURRENT_DIR=$(pwd)
    PG_USER="postgres"

    # Clean the old PostgreSQL installation
    kill_postgres_process

    # Make and install PostgreSQL
    cd "${POSTGRESQL_PATH}"
    bash ./configure
    make install
    cd "$CURRENT_DIR"

    # Initialize the database
    rm -rf ${PG_INSTALL_PATH}/data
    mkdir -p ${PG_INSTALL_PATH}/data
    chown -R ${PG_USER}:${PG_USER} ${PG_INSTALL_PATH}
    su - ${PG_USER} -c "${PG_INSTALL_PATH}/bin/initdb -D ${PG_INSTALL_PATH}/data"

    # Edit the postgresql.conf file
    configure_postgres "${PG_INSTALL_PATH}"

    # Start the PostgreSQL server
    su - ${PG_USER} -c "${PG_INSTALL_PATH}/bin/pg_ctl -D ${PG_INSTALL_PATH}/data -l ${PG_INSTALL_PATH}/logfile start"
}

install_postgres "$1" "$2"
