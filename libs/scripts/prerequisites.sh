#!/bin/bash
set -e

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#! MAIN:
#!   Install prerequisites for PostgreSQL on a Grid'5000 node.
#!
#! AUTHORS:
#!   Steven Gourves
#!
#! USAGE:
#!   ./prerequisites.sh <disk> <pg_install_path>
#!
#! PARAMETERS:
#!   <disk>              : Grid'5000 disk
#!   <pg_install_path>   : Path of the PostgreSQL installation directory
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

mount_g5k_disk() {
    # Parameters
    DISK="$1"
    PG_INSTALL_PATH="$2"

    # Mount the disk if we are on a Grid'5000 node
    if [[ $(hostname) == *"grid5000"* ]]; then
        # Check if partition /dev/${DISK}p1 exists
        if [ ! -e /dev/${DISK}p1 ]; then
            echo "The disk partition /dev/${DISK}p1 doesn't exist."

            # If the disk contains a signature, remove it
            if [ -e /dev/${DISK} ]; then
                wipefs -a /dev/${DISK}
            fi

            # Create a partition on the disk called ${DISK}p1
            echo -e "n\np\n1\n\n\nw" | fdisk /dev/${DISK}
        fi

        mkfs -t ext4 /dev/${DISK}p1 || true
        mount /dev/${DISK}p1 ${PG_INSTALL_PATH}
    fi
}

prerequisites() {
    # Parameters
    DISK="$1"
    PG_INSTALL_PATH="$2"

    # Variables
    PGSQL_USER=postgres
    PGSQL_PASSWORD=postgres

    # Check if this file is executed with root privileges
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be executed with root privileges"
        exit 1
    fi

    # Apt packages
    apt update
    apt-get install libreadline-dev bison flex zlib1g-dev make rpm libpq-dev python3-pip fdisk -y

    # Python packages
    pip3 install psycopg2 requests pandas

    # PostgreSQL user (if not exists)
    if ! id -u ${PGSQL_USER} >/dev/null 2>&1; then
        useradd -m -d ${PG_INSTALL_PATH} ${PGSQL_USER}
        echo -e "${PGSQL_PASSWORD}\n${PGSQL_PASSWORD}" | passwd ${PGSQL_USER}
    fi

    # Create the PostgreSQL data directory
    mkdir -p ${PG_INSTALL_PATH}

    # Mount the disk if we are on a Grid'5000 node
    mount_g5k_disk "${DISK}" "${PG_INSTALL_PATH}"
}

prerequisites "$1" "$2"
