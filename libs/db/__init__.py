"""Python module to manage databases.

Classes:
    Database: Class to manage databases.
"""

# Import classes
from .database import Database
