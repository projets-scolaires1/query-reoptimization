import psycopg2


class Database:
    """Class to manage databases.

    Methods:
        execute: Execute a query.
        close: Close the connection.

    Attributes:
        _connection (psycopg2.extensions.connection): Connection to the database.

    Usage:
        from libs.db import Database

        db = Database(db_name, db_user, db_password, db_host, db_port)
        db.execute(query)
        db.close()
    """

    def __init__(self, db_name: str, db_user: str, db_password: str, db_port: str, db_host: str = "localhost") -> None:
        """Initialize the class.

        Args:
            db_name (str): Database name.
            db_user (str): User of the database.
            db_password (str): Password of the database.
            db_port (str): Port of the database.
            db_host (str, optional): Host of the database. Defaults to "localhost".
        """
        self._connection = psycopg2.connect(
            database=db_name, user=db_user,
            password=db_password, host=db_host, port=db_port)
        self._connection.autocommit = True

    def execute(self, query: str) -> None:
        """Execute a query.

        Args:
            query (str): Query to execute.
        """
        cursor = self._connection.cursor()
        cursor.execute(query)
        cursor.close()

    def close(self) -> None:
        """Close the connection."""
        self._connection.close()
