"""Python module to manage Grid'5000.

Classes:
    G5kNode: Class to manage a Grid'5000 node.
"""

# Import classes
from .node import G5kNode
