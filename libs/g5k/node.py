import os
import logging
import paramiko
from scp import SCPClient

G5K_HOST = "access.grid5000.fr"


class G5kNode:
    """Class to manage a Grid'5000 node.

    Methods:
        _connect: Connect to a Grid'5000 site.
        reserve: Reserve a node on Grid'5000.
        _reserve_node_disk: Reserve a node disk on Grid'5000.
        _reserve_node: Reserve a node on Grid'5000.
        release: Release reservations.
        get_results: Get results from the site.

    Attributes:
        _g5k_user (str): Grid'5000 username.
        _g5k_pkey (str): Path to the Grid'5000 private key.
        _node (str): Grid'5000 node.
        _site (str): Grid'5000 site.
        _node_port (int): SSH port.
        _node_disk (str): Grid'5000 disk for the node.
        _node_host (str): Node host.
        _node_user (str): Node user.
        _node_reservation_id (str): Node reservation ID.
        _node_disk_reservation_id (str): Node disk reservation ID.

    Usage:
        from libs.g5k import G5kNode

        g5k_node = G5kNode(g5k_user, g5k_pkey, node, site, port, disk)
        g5k_node.reserve(walltime, script, start_time)
        g5k_node.release()
        g5k_node.get_results(remote_path, local_path)
    """

    def __init__(self, g5k_user: str, g5k_pkey: str, node: str, site: str, port: int = 22, disk: str = "disk1") -> None:
        """Initialize the class.

        Args:
            g5k_user (str): Grid'5000 username.
            g5k_pkey (str): Path to the Grid'5000 private key.
            node (str): Grid'5000 node.
            site (str): Grid'5000 site.
            port (int, optional): SSH port. Defaults to 22.
            disk (str, optional): Grid'5000 disk for the node. Defaults to "disk1".
        """
        self._g5k_user = g5k_user
        self._g5k_pkey = g5k_pkey
        self._node = node
        self._site = site
        self._node_port = port
        self._node_disk = disk
        self._node_host = f"{self._node}.{self._site}"
        self._node_user = "root"
        self._node_reservation_id = None
        self._node_disk_reservation_id = None

    def _connect(self, site: str, site_user: str, site_pkey: str, port: int = 22) -> paramiko.SSHClient:
        """Connect to a Grid'5000 site.

        Args:
            site (str): Grid'5000 site.
            site_user (str): Grid'5000 site username.
            site_pkey (str): Path to the Grid'5000 site private key.
            port (int, optional): SSH port. Defaults to 22.

        Returns:
            paramiko.SSHClient: SSH client.
        """

        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        proxy = paramiko.ProxyCommand(
            f"ssh -i {self._g5k_pkey} {self._g5k_user}@{G5K_HOST} -W \"{site}:{port}\"")
        # proxy.settimeout(10)

        client.connect(
            hostname=site,
            username=site_user,
            key_filename=site_pkey,
            sock=proxy)

        return client

    def reserve(self, walltime: str, script: str, start_time: str = None) -> None:
        """Reserve a node on Grid'5000.

        Args:
            walltime (str): Walltime for the reservation.
            script (str): Script to execute on the node.
            start_time (str): Start time for the reservation. Defaults to now.

        Raises:
            Exception: Error while reserving a node.
        """
        logging.debug(f"Grid'5000 reservation for {self._node} node...")

        # Get current time of UTC+1 in the following format: YYYY-MM-DD HH:MM:SS
        if start_time is None:
            start_time = os.popen(
                "TZ='Europe/Paris' date +'%Y-%m-%d %H:%M:%S'").read().strip()

        # Connect to the Grid'5000 site
        ssh_site = self._connect(self._site, self._g5k_user, self._g5k_pkey)

        # Copy g5k_init.sh script to the site
        # TODO: Put the script path in parameters
        logging.debug(f"Copying g5k_init.sh script to {self._site}...")
        scp = SCPClient(ssh_site.get_transport())
        scp.put("libs/g5k/g5k_init.sh", "g5k_init.sh")
        scp.close()

        # Reservations
        self._reserve_node_disk(ssh_site, walltime, start_time)
        self._reserve_node(ssh_site, walltime, script, start_time)

        # Close connection
        ssh_site.close()

    def _reserve_node_disk(self, ssh_site: paramiko.SSHClient, walltime: str, start_time: str = None) -> None:
        """Reserve a node disk on Grid'5000.

        Args:
            ssh_site (paramiko.SSHClient): SSH client.
            walltime (str): Walltime for the reservation.
            start_time (str): Start time for the reservation. Defaults to now.
        """
        logging.debug(
            f"Node disk {self._node_disk}.{self._node} reservation...")

        # Get current time of UTC+1 in the following format: YYYY-MM-DD HH:MM:SS
        if start_time is None:
            start_time = os.popen(
                "TZ='Europe/Paris' date +'%Y-%m-%d %H:%M:%S'").read().strip()

        oarsub_cmd = f'oarsub -r "{start_time}" -t noop -l "{{disk & {self._node} and disk in ({self._node_disk}.{self._node})}}"/host=1/disk=1,walltime={walltime}'

        _, stdout, _ = ssh_site.exec_command(oarsub_cmd)
        output = stdout.readlines()

        # Check if contains KO, if yes, put a warning
        if any("KO" in line for line in output):
            logging.warning(
                f"Reservation of the disk {self._node_disk} failed")
            logging.debug(f"Output: {output}")
        else:
            # Get the reservation ID
            job_id = [line.strip().split("=")[1]
                      for line in output if "OAR_JOB_ID" in line]
            self._node_disk_reservation_id = job_id[0]
            logging.info(
                f"Node disk {self._node_disk}.{self._node} reserved, with ID {self._node_disk_reservation_id}")

    def _reserve_node(self, ssh_site: paramiko.SSHClient, walltime: str, script: str, start_time: str = None) -> None:
        """Reserve a node disk on Grid'5000.

        Args:
            ssh_site (paramiko.SSHClient): SSH client.
            walltime (str): Walltime for the reservation.
            script (str): Script to execute on the node.
            start_time (str): Start time for the reservation. Defaults to now.
        """
        logging.debug(f"Node {self._node_disk} reservation...")

        # Get current time of UTC+1 in the following format: YYYY-MM-DD HH:MM:SS
        if start_time is None:
            start_time = os.popen(
                "TZ='Europe/Paris' date +'%Y-%m-%d %H:%M:%S'").read().strip()

        oarsub_cmd = f'oarsub -r "{start_time}" -l "{{host in ({self._node})}}"/host=1,walltime={walltime} "{script}"'

        _, stdout, _ = ssh_site.exec_command(oarsub_cmd)
        output = stdout.readlines()

        # Check if contains KO, if yes, generate an error
        if any("KO" in line for line in output):
            logging.error(
                f"Reservation of the node {self._node} failed")
            logging.debug(f"Output: {output}")
            self.release()
            raise Exception(f"Reservation of the node {self._node} failed")
        else:
            # Get the reservation ID
            job_id = [line.strip().split("=")[1]
                      for line in output if "OAR_JOB_ID" in line]
            self._node_reservation_id = job_id[0]
            logging.info(
                f"Node {self._node} reserved, with ID {self._node_reservation_id}")

    def release(self) -> None:
        """Release reservations.

        Raises:
            Exception: Error while releasing reservations.
        """
        logging.debug("Releasing reservations...")

        # Connection to the Grid'5000 site
        ssh_site = self._connect(
            self._site, self._g5k_user, self._g5k_pkey)

        for res_id in [self._node_disk_reservation_id, self._node_reservation_id]:
            if res_id is None:
                continue

            _, stdout, stderr = ssh_site.exec_command(f"oardel {res_id}")

            if stdout.channel.recv_exit_status() != 0:
                ssh_site.close()
                logging.error(
                    f"Error while releasing reservation {res_id}. Output[stdout]: {stdout.readlines()}. Error[stderr]: {stderr.readlines()}")
                raise Exception(
                    f"Error while releasing reservation {res_id}. Output[stdout]: {stdout.readlines()}. Error[stderr]: {stderr.readlines()}")
            else:
                logging.info(f"Reservation {res_id} released.")

        # Close connection
        ssh_site.close()

    def get_results(self, remote_path: str, local_path: str) -> None:
        """Get results from the site.

        Args:
            remote_path (str): Remote path to get the results.
            local_path (str): Local path to save the results.
        """
        # Connect to the Grid'5000 site
        ssh_site = self._connect(self._site, self._g5k_user, self._g5k_pkey)

        # Copy results from the site
        logging.debug(f"Copying results from {self._site}...")
        scp = SCPClient(ssh_site.get_transport())
        scp.get(remote_path, local_path, recursive=True)
        scp.close()

        # Close connection
        ssh_site.close()
