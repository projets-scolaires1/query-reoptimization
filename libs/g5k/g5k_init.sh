#!/bin/bash
set -e

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#! MAIN:
#!   Main script for the Grid'5000 node's execution test.
#!
#! AUTHORS:
#!   Steven Gourves
#!
#! USAGE:
#!   ./g5k_execution.sh <branch> <git_project_url> <node_exec_path> \
#!      <algorithm> <abs_output_path> <scale> <queries> <trials> \
#!      <site> <disk> <pg_data_path> <tpch_queries_path> <tests_type> \
#!      <nb_connections>
#!
#! PARAMETERS:
#!   <branch>            : Branch of the git project to clone
#!   <git_project_url>   : URL of the git project to clone
#!   <node_exec_path>    : Path of the git project to clone
#!   <algorithm>         : Algorithm to execute
#!   <abs_output_path>   : Absolute path of the output directory
#!   <scale>             : Scale factor
#!   <queries>           : Number of queries
#!   <trials>            : Number of trials
#!   <site>              : Grid'5000 site
#!   <disk>              : Grid'5000 disk
#!   <pg_data_path>      : Path of the PostgreSQL data directory
#!   <tpch_queries_path> : Path of the TPC-H queries directory
#!   <tests_type>        : Type of tests to execute (e.g. serial or parallel)
#!   <nb_connections>    : Number of connections (only for tests_type 'parallel')
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

check_parameters() {
    # Parameters
    BRANCH="$1"
    GIT_PROJECT_URL="$2"
    NODE_EXEC_PATH="$3"
    ALGORITHM="$4"
    ABS_OUTPUT_PATH="$5"
    SCALE="$6"
    QUERIES="$7"
    TRIALS="$8"
    SITE="$9"
    DISK="${10}"
    PG_DATA_PATH="${11}"
    TPCH_QUERIES_PATH="${12}"
    TESTS_TYPE="${13}"
    NB_CONNECTIONS="${14}"

    # Check parameters
    if [ -z ${BRANCH} ] || [ -z ${GIT_PROJECT_URL} ] || [ -z ${NODE_EXEC_PATH} ] || [ -z ${ALGORITHM} ] || [ -z ${ABS_OUTPUT_PATH} ] || [ -z ${SCALE} ] || [ -z ${QUERIES} ] || [ -z ${TRIALS} ] || [ -z ${SITE} ] || [ -z ${DISK} ] || [ -z ${PG_DATA_PATH} ] || [ -z ${TPCH_QUERIES_PATH} ] || [ -z ${TESTS_TYPE} ] || [ -z ${NB_CONNECTIONS} ]; then
        echo "ERROR: Missing parameters. The followings parameters need to be defined: BRANCH GIT_PROJECT_URL NODE_EXEC_PATH ALGORITHM ABS_OUTPUT_PATH SCALE QUERIES TRIALS SITE DISK PG_DATA_PATH TPCH_QUERIES_PATH TESTS_TYPE NB_CONNECTIONS."
        exit 1
    fi
}

get_git_project() {
    # Parameters
    GIT_PROJECT_URL="$1"
    BRANCH="$2"
    PROJECT_PATH="$3"

    # If the folder exists and contains `.git`, get the branch and update it
    # Else, clone the repository
    if [ -d ${PROJECT_PATH} ] && [ -d ${PROJECT_PATH}/.git ]; then
        cd ${PROJECT_PATH}
        git fetch --prune
        git checkout ${BRANCH}
        git pull
    else
        rm -rf ${PROJECT_PATH}
        git clone --branch ${BRANCH} ${GIT_PROJECT_URL}.git ${PROJECT_PATH}
        cd ${PROJECT_PATH}
    fi
}

g5k_execution() {
    # Parameters
    BRANCH="$1"
    GIT_PROJECT_URL="$2"
    NODE_EXEC_PATH="$3"
    ALGORITHM="$4"
    ABS_OUTPUT_PATH="$5"
    SCALE="$6"
    QUERIES="$7"
    TRIALS="$8"
    SITE="$9"
    DISK="${10}"
    PG_DATA_PATH="${11}"
    TPCH_QUERIES_PATH="${12}"
    TESTS_TYPE="${13}"
    NB_CONNECTIONS="${14}"

    # Variables
    CURRENT_DIR="$(pwd)"

    # Check parameters
    check_parameters ${BRANCH} ${GIT_PROJECT_URL} ${NODE_EXEC_PATH} ${ALGORITHM} ${ABS_OUTPUT_PATH} ${SCALE} ${QUERIES} ${TRIALS} ${SITE} ${DISK} ${PG_DATA_PATH} ${TPCH_QUERIES_PATH} ${TESTS_TYPE} ${NB_CONNECTIONS}

    # 1. Get the project from Git (git clone)
    echo "G5K_INIT: 1. Get the project from Git (git clone)"
    get_git_project ${GIT_PROJECT_URL} ${BRANCH} ${NODE_EXEC_PATH}

    # 2. Load TPC-H queries if exist
    echo "G5K_INIT: 2. Load TPC-H queries if exist"
    if [ -d ${TPCH_QUERIES_PATH} ]; then
        cd ~
        mkdir -p ${ABS_OUTPUT_PATH}
        cp -r ${TPCH_QUERIES_PATH} ${ABS_OUTPUT_PATH}/queries_${QUERIES}
        cd ${NODE_EXEC_PATH}
    fi

    # 3. Setup environment (prerequisites and PostgreSQL)
    echo "G5K_INIT: 3. Setup environment (prerequisites and PostgreSQL)"
    python3 setup.py -o ${ABS_OUTPUT_PATH} -p ${PG_DATA_PATH} -d ${DISK} -a ${ALGORITHM} -s ${SCALE} -q ${QUERIES}

    # 4. Execute tests
    echo "G5K_INIT: 4. Execute tests"
    python3 run.py -o ${ABS_OUTPUT_PATH} -a ${ALGORITHM} -s ${SCALE} -q ${QUERIES} -t ${TRIALS} -T ${TESTS_TYPE} -c ${NB_CONNECTIONS}

    # 5. Get results on the Grid'5000 site
    echo "G5K_INIT: 5. Get results on the Grid'5000 site"
    mkdir -p ${CURRENT_DIR}/results
    cp -r ${ABS_OUTPUT_PATH}/results/* ${CURRENT_DIR}/results/

    echo "G5K_INIT: Done"
}

g5k_execution "$1" "$2" "$3" "$4" "$5" "$6" "$7" "$8" "$9" "${10}" "${11}" "${12}" "${13}" "${14}"
