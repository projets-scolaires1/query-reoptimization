import os
import logging
import pandas as pd


class ResAverage:
    """Class to calculate the average of the results.

    Methods:
        average_by_query_type: Calculate the average (execution time and monetary cost) by query type.

    Attributes:
        None

    Usage:
        from libs.results import ResAverage

        ResAverage.average_by_query_type(results_path)
    """

    @staticmethod
    def average_by_query_type(results_path):
        """Calculate the average (execution time and monetary cost) by query type.

        Args:
            results_path (str): Path to the results.
        """
        # Initialize a dictionary to store the data
        results = {i: {'total_time': 0.0, 'total_cost': 0.0, 'count': 0.0}
                   for i in range(22)}

        trials = next(os.walk(results_path))[1]

        # Calculate the average
        for trial in trials:
            csv_files = os.listdir(f"{results_path}/{trial}")

            for file in csv_files:
                df = pd.read_csv(
                    f"{results_path}/{trial}/{file}", header=None, names=[
                        'query_type', 'query_id', 'time_of_execution', 'monetary_cost'])
                df = df.iloc[1:]  # Remove the first row (header)

                for _, row in df.iterrows():
                    query_type = int(row['query_type']) - 1

                    # Update results
                    results[query_type]['total_time'] += float(
                        row['time_of_execution'])
                    results[query_type]['total_cost'] += float(
                        row['monetary_cost'])
                    results[query_type]['count'] += 1

        # Calculate the averages
        for query_type, data in results.items():
            data['average_time'] = data['total_time'] / data['count']
            data['average_cost'] = data['total_cost'] / data['count']
            logging.debug(
                f"Average by query type: Q{query_type+1}, {data['average_time']}ms, {data['average_cost']}$")

        # Create a DataFrame for the results
        result_to_save = pd.DataFrame(
            [(f'Q{query_type+1}', data['average_time'], data['average_cost'])
             for query_type, data in results.items()],
            columns=['Query', 'Average execution time', 'Average monetary cost'])

        # Save the results to a new CSV file
        result_to_save.to_csv(
            f"{results_path}/average_by_query_type.csv", index=False)

        logging.info(
            f"Average by query type: {results_path}/average_by_query_type.csv")
