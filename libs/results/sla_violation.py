import os
import logging
import pandas as pd

# TODO: 16000.0 is only for 50GB? Need to be adjusted with database scale?
SLA_LIMIT_REF = 320.0


class ResSLAViolation:
    """Class to calculate the SLA violation of the results.

    Methods:
        sla_violation: Calculate the SLA violation.

    Attributes:
        None

    Usage:
        from libs.results import ResSLAViolation

        ResSLAViolation.sla_violation(results_path, scale, nb_queries)
    """

    @staticmethod
    def sla_violation(results_path: str, scale: float, nb_queries: int):
        """Calculate the SLA violation.

        Args:
            results_path (str): Path to the results.
            scale (float): Scale factor.
            nb_queries (int): Number of queries.
        """
        sla_limit = SLA_LIMIT_REF * scale
        nb_sla_violations = 0.0
        sla_violation_rate = 0.0

        trials = next(os.walk(results_path))[1]

        # Calculate the average
        calc_nb_q = 0
        for trial in trials:  # trials
            csv_files = os.listdir(f"{results_path}/{trial}")

            for file in csv_files:  # groups
                df = pd.read_csv(
                    f"{results_path}/{trial}/{file}", header=None, names=[
                        'query_type', 'query_id', 'execution_time', 'monetary_cost'])
                df = df.iloc[1:]  # Remove the first row (header)

                for _, row in df.iterrows():  # queries
                    calc_nb_q += 1
                    if float(row['execution_time']) > sla_limit:
                        nb_sla_violations += 1

        nb_sla_violations /= len(trials)

        # Calculate the SLA violation rate
        sla_violation_rate = nb_sla_violations / nb_queries * 100

        # Save the results to a json file
        results = {
            "nb_sla_violations": nb_sla_violations,
            "sla_violation_rate": sla_violation_rate
        }
        pd.DataFrame([results]).to_json(
            f"{results_path}/sla_violation.json", orient="records")

        logging.debug(f"SLA limit: {sla_limit}ms")
        logging.debug(
            f"Number of SLA violations: {nb_sla_violations} (average of each trial)")
        logging.info(f"SLA violation rate: {sla_violation_rate}%")
