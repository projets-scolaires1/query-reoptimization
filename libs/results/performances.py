import os
import logging
import numpy as np
import pandas as pd


class ResPerformances:
    """Class to calculate the Standard Deviation and the average of the results.

    Methods:
        performances: Generate JSON file with the results (averages and standard deviation)

    Attributes:
        None

    Usage:
        from libs.results import ResPerformances

        ResPerformances.performances(results_path)
    """

    @staticmethod
    def performances(results_path: str, nb_connections: int) -> None:
        """Generate JSON file with the results (averages and standard deviation)

        Args:
            results_path (str): Path to the results.
            nb_connections (int): Number of connections.
        """
        trials_execution_times = []
        trials_monetary_costs = []

        trials = next(os.walk(results_path))[1]

        # Calculate the average
        i_trial = 0
        for trial in trials:  # For each trial
            csv_files = os.listdir(f"{results_path}/{trial}")

            trials_execution_times.append([])
            trials_monetary_costs.append([])

            for file in csv_files:  # For each queries group execution results
                df = pd.read_csv(
                    f"{results_path}/{trial}/{file}", header=None, names=[
                        'query_type', 'query_id', 'execution_time', 'monetary_cost'])
                df = df.iloc[1:]

                for _, row in df.iterrows():  # For each query in the group
                    trials_execution_times[i_trial].append(
                        float(row['execution_time']))
                    trials_monetary_costs[i_trial].append(
                        float(row['monetary_cost']))

            i_trial += 1

        # Average of each trial
        avg_execution_times = []
        avg_monetary_costs = []

        for i in range(len(trials_execution_times[0])):
            sum_execution_times = sum([
                trials_execution_times[j][i] for j in range(len(trials_execution_times))])
            avg_execution_times.append(
                sum_execution_times / len(trials_execution_times))

            sum_monetary_costs = sum([
                trials_monetary_costs[j][i] for j in range(len(trials_monetary_costs))])
            avg_monetary_costs.append(
                sum_monetary_costs / len(trials_monetary_costs))

        # Average query
        avg_query_execution_time = np.mean(avg_execution_times)
        avg_query_monetary_cost = np.mean(avg_monetary_costs)

        # Average standard deviation
        avg_std_deviation_execution_time = np.std(avg_execution_times)
        avg_std_deviation_monetary_cost = np.std(avg_monetary_costs)

        # Get the average time to execute a trial
        with open(f"{results_path}/execution_times.json", 'r') as file:
            trials_execution_time = pd.read_json(file)
        avg_trial_execution_time = np.mean(trials_execution_time)

        # If the tests_type is 'serial', nb_connections = 1
        nb_connections = 1 if "serial" in results_path else nb_connections

        # Create a DataFrame for the results
        results = {
            'avg_query_execution_time': avg_query_execution_time,
            'avg_query_monetary_cost': avg_query_monetary_cost,
            'avg_std_deviation_execution_time': avg_std_deviation_execution_time,
            'avg_std_deviation_monetary_cost': avg_std_deviation_monetary_cost,
            'avg_trial_execution_time': avg_trial_execution_time,
            'nb_connections': nb_connections}

        # Save the results to a new JSON file
        pd.DataFrame([results]).to_json(
            f"{results_path}/performances.json", orient="records")

        # Print the results
        logging.info("STD_DEVIATION: Results generated")
        logging.debug(
            f"Average query execution time: {avg_query_execution_time}")
        logging.debug(
            f"Average query monetary cost: {avg_query_monetary_cost}")
        logging.debug(
            f"Average standard deviation execution time: {avg_std_deviation_execution_time}")
        logging.debug(
            f"Average standard deviation monetary cost: {avg_std_deviation_monetary_cost}")
        logging.debug(
            f"Average trial execution time: {avg_trial_execution_time}")
