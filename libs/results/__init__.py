"""Python module to manage the results of the experiments.

Classes:
    ResAverage: Class to calculate the average of the results.
    ResSLAViolation: Class to calculate the SLA violation of the results.
    ResPerformances: Class to calculate the Standard Deviation and the average of the results.
"""

# Import classes
from .averages import ResAverage
from .sla_violation import ResSLAViolation
from .performances import ResPerformances
