import csv
import logging
from multiprocessing import Process, Queue
import os
import shutil
from threading import Event, Lock
import time

from libs.db import Database


class RunQueries:
    """Class to run TPC-H Benchmark queries on database.

    Methods:
        run_serial: Run queries in a serial execution on only one connection.
        run_parallel: Run queries distributed across parallel connections (with Threads).
        _get_queries: Get all queries.
        _execute_queries_from_queue: Execute queries from a queue and save results to a CSV file.
        _execute_query: Execute a query from a SQL file.

    Attributes:
        _db_name (str): Database name.
        _db_user (str): User of the database.
        _db_password (str): Password of the database.
        _db_host (str): Host of the database.
        _db_port (str): Port of the database.
        _hourly_rate (float): Hourly rate for monetary costs.

    Usage:
        from libs.tpch import Queries

        queries = Queries(db_name, db_user, db_password, db_host, db_port)
        queries.run_parallel(queries_path, output_folder, nb_connections, nb_trials)
    """

    def __init__(self, db_name: str = "tpch", db_user: str = "postgres", db_password: str = "postgres", db_host: str = "localhost", db_port: str = "5432", hourly_rate: float = 0.00012) -> None:
        """Initialize the class.

        Args:
            db_name (str, optional): Database name. Defaults to "tpch".
            db_user (str, optional): User of the database. Defaults to "postgres".
            db_password (str, optional): Password of the database. Defaults to "postgres".
            db_host (str, optional): Host of the database. Defaults to "localhost".
            db_port (str, optional): Port of the database. Defaults to "5432".
            hourly_rate (float, optional): Hourly rate for monetary costs. Defaults to 0.00012.
        """
        self._db_name = db_name
        self._db_user = db_user
        self._db_password = db_password
        self._db_host = db_host
        self._db_port = db_port
        self._hourly_rate = hourly_rate

    def run_serial(self, queries_path: str, output_folder: str, nb_trials: int = 1) -> None:
        """Run queries in a serial execution on only one connection.

        Args:
            queries_path (str): Directory path containing the queries.
            output_folder (str): Path of the output file.
            nb_trials (int, optional): Number of trials. Defaults to 1.
        """
        if os.path.exists(output_folder):
            shutil.rmtree(output_folder)
        os.makedirs(output_folder)

        # Get all query groups ordered by number
        groups = os.listdir(queries_path)
        groups.sort(key=lambda x: int(x.split('_')[-1]))

        # Get execution time for each trial
        execution_times = []

        for trial in range(1, nb_trials + 1):
            output_trial_folder = f"{output_folder}/trial_{trial}"
            os.makedirs(output_trial_folder)

            # Start execution time
            start = time.time()

            db = Database(
                db_name=self._db_name, db_user=self._db_user,
                db_password=self._db_password, db_host=self._db_host, db_port=self._db_port)

            for group in groups:
                output_file = f"{output_trial_folder}/output_{group}.csv"

                # Initialize the output file with header
                results_file = open(output_file, 'w', newline='')
                csv_writer = csv.writer(results_file)
                csv_writer.writerow([
                    'Query Type', 'Query ID',
                    'Execution Time (ms)', 'Monetary Cost ($)'])

                # Get all queries ordered by number
                queries_group = os.listdir(f"{queries_path}/{group}")
                queries_group.sort(key=lambda x: int(
                    x.split('-')[1].split('.')[0]))

                for query_file in queries_group:
                    query_type, query_id = query_file.split(".")[0].split("-")
                    sql_file = f"{queries_path}/{group}/{query_file}"

                    # Execute query
                    execution_time, monetary_cost = self._execute_query(db,
                                                                        sql_file)

                    # Write results:
                    csv_writer.writerow([
                        query_type, query_id,
                        execution_time, monetary_cost])

                results_file.close()

            db.close()

            # End execution time
            end = time.time()
            execution_times.append((end - start) * 1000)

        # Write execution times in JSON format file
        with open(f"{output_folder}/execution_times.json", 'w') as file:
            file.write(str(execution_times))

    def run_parallel(self, queries_path: str, output_folder: str, nb_connections: int = 2, nb_trials: int = 1) -> None:
        """Run queries distributed across parallel connections.

        To distribute queries across connections, we need to recover all queries and put them in a queue that will be consumed by processes.

        Args:
            queries_path (str): Directory path containing the queries.
            output_folder (str): Path of the output file.
            nb_connections (int, optional): Number of connections in parallel (to execute the queries). Defaults to 2.
            nb_trials (int, optional): Number of trials. Defaults to 1.
        """
        if os.path.exists(output_folder):
            shutil.rmtree(output_folder)
        os.makedirs(output_folder)

        # Process lock to write results
        # As we can't know which file will be written by the processes,
        # we need to lock the access to the output files to avoid conflicts
        write_access = Lock()

        # Get execution time for each trial
        execution_times = []

        for t in range(1, nb_trials + 1):
            # Get all queries
            queries = self._get_queries(queries_path, output_folder, t)

            # Process queue for queries
            queries_queue = Queue()
            for query in queries:
                queries_queue.put(query)

            # Create Processes/Connections to execute queries
            connections = []
            for _ in range(nb_connections):
                process = Process(target=self._execute_queries_from_queue, args=(
                    queries_queue, write_access))
                connections.append(process)

            # Start processes/connections
            start = time.time()
            for process in connections:
                process.start()

            # Wait for all processes/connections to complete (all queries executed)
            for process in connections:
                process.join()

            # End execution time
            end = time.time()
            execution_times.append((end - start) * 1000)

        # Write execution times in JSON format file
        with open(f"{output_folder}/execution_times.json", 'w') as file:
            file.write(str(execution_times))

    def _get_queries(self, queries_path: str, output_folder_base: str, trial: int) -> list:
        """Get all queries.

        Obtain all queries from the queries_path. It returns a list of queries in the following format:
        > [[query_id, query_type, sql_file, output_file], ...]

        The output_file is the path of the CSV file where the results will be written. This file will be created if it doesn't exist and initialized with the header.

        Args:
            queries_path (str): Directory path containing the queries.
            output_folder_base (str): Path of the output folder.
            trial (int): Number of the trial to target.

        Returns:
            list: List of queries.
        """
        # Get all query groups ordered by number
        groups = os.listdir(queries_path)
        groups.sort(key=lambda x: int(x.split('_')[-1]))

        # Get all queries
        queries = []

        # Create output folder for the trial
        output_trial_folder = f"{output_folder_base}/trial_{trial}"
        os.makedirs(output_trial_folder)

        for group in groups:
            output_file = f"{output_trial_folder}/output_{group}.csv"

            # Initialize the output file with header
            with open(output_file, 'w') as file:
                file.write(
                    "Query Type, Query ID, Execution Time (ms), Monetary Cost ($)\n")

            # Get all queries ordered by number
            queries_group = os.listdir(f"{queries_path}/{group}")
            queries_group.sort(key=lambda x: int(
                x.split('-')[1].split('.')[0]))

            for query_file in queries_group:
                query_type, query_id = query_file.split(".")[0].split("-")
                sql_file = f"{queries_path}/{group}/{query_file}"

                queries.append(
                    [query_id, query_type, sql_file, output_file])

        return queries

    def _execute_queries_from_queue(self, queries_queue: Queue, write_access: Event) -> None:
        """Execute queries from a queue and save results to a CSV file.

        Args:
            queries_queue (Queue): Queue contains queries.
            write_access (Event): Event to write results.
        """
        # Database connection
        db = Database(
            db_name=self._db_name, db_user=self._db_user,
            db_password=self._db_password, db_host=self._db_host, db_port=self._db_port)

        # Get queries from queue
        while not queries_queue.empty():
            query_data = queries_queue.get()
            query_id = query_data[0]
            query_type = query_data[1]
            sql_file = query_data[2]
            output_file = query_data[3]

            # Execute query
            execution_time, monetary_cost = self._execute_query(db, sql_file)

            # Write results in the following format:
            # > query_type, query_id, execution_time, monetary_cost
            with write_access:
                with open(output_file, 'a') as file:
                    file.write(
                        f"{query_type},{query_id},{execution_time},{monetary_cost}\n")

        db.close()

    def _execute_query(self, db: Database, sql_file: str) -> tuple:
        """Execute a query from a SQL file.

        Args:
            db (Database): Database connection.
            sql_file (str): Path of the SQL file.

        Returns:
            tuple: Execution time and monetary cost.
        """
        # Get query from SQL file
        query = open(sql_file, 'r').read()

        # Execute query and measure execution time
        start = time.time()
        db.execute(query)
        end = time.time()
        execution_time = (end - start) * 1000  # In milliseconds
        monetary_cost = self._hourly_rate * execution_time

        logging.info(f"{sql_file}: {execution_time} ms, {monetary_cost}$")

        return execution_time, monetary_cost
