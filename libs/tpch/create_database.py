import os
import psycopg2
import logging

TABLES = ["region", "nation", "supplier", "customer",
          "part", "partsupp", "orders", "lineitem"]


class CreateDatabase:
    """Class to create TPC-H Benchmark database on PostgreSQL.

    Methods:
        create_database: Create database.
        create_tables: Create tables.
        populate_tables: Populate tables.
        create_indexes: Create indexes.

    Attributes:
        _db_name (str): Database name.
        _db_user (str): User of the database.
        _db_password (str): Password of the database.
        _db_host (str): Host of the database.
        _db_port (str): Port of the database.

    Usage:
        from libs.tpch import CreateDatabase

        psql_db = CreateDatabase(db_name, db_user, db_password, db_host, db_port)
        psql_db.create_database()
        psql_db.create_tables()
        psql_db.populate_tables(data_path="./output/data")
        psql_db.create_indexes()
    """

    def __init__(self, db_name: str = "tpch", db_user: str = "postgres", db_password: str = "postgres", db_host: str = "localhost", db_port: str = "5432") -> None:
        """Initialize the class.

        Args:
            db_name (str, optional): Database name. Defaults to "tpch".
            db_user (str, optional): User of the database. Defaults to "postgres".
            db_password (str, optional): Password of the database. Defaults to "postgres".
            db_host (str, optional): Host of the database. Defaults to "localhost".
            db_port (str, optional): Port of the database. Defaults to "5432".
        """
        self._db_name = db_name
        self._db_user = db_user
        self._db_password = db_password
        self._db_host = db_host
        self._db_port = db_port

    def create_database(self, db_template_name: str = "template1") -> None:
        """Create database.

        If the database already exists, it will be dropped and recreated.

        Args:
            db_template_name (str): Database template name. Default: "template1"
        """
        # Establishing the connection
        conn = psycopg2.connect(
            database=db_template_name, user=self._db_user,
            password=self._db_password, host=self._db_host, port=self._db_port)
        conn.autocommit = True

        # Create database
        cursor = conn.cursor()
        cursor.execute(f"DROP DATABASE IF EXISTS {self._db_name}")
        cursor.execute(f"CREATE DATABASE {self._db_name};")
        cursor.close()

        conn.close()

        logging.debug("DATABASE: DB created")

    def create_tables(self, tables_schema_path: str = "./libs/tpch/schema/tpch_schema.sql") -> None:
        """Create tables.

        Args:
            tables_schema_path (str): Path of the schema file. Default: "./libs/tpch/schema/tpch_schema.sql"
        """
        # Establishing the connection
        conn = psycopg2.connect(
            database=self._db_name, user=self._db_user,
            password=self._db_password, host=self._db_host, port=self._db_port)
        conn.autocommit = True

        # Create tables
        cursor = conn.cursor()
        with open(tables_schema_path) as f:
            dbgen = f.read()
            f.close()
        cursor.execute(dbgen)

        cursor.close()
        conn.close()

        logging.debug("DATABASE: Tables created")

    def populate_tables(self, data_path: str) -> None:
        """Populate database with .csv files.

        It use `\copy` tool from PostgreSQL to populate database with .csv files.

        Args:
            data_path (str): Path of the data directory. Default: "./data"
        """
        # Check if data_dir exists
        if not os.path.isdir(data_path):
            raise Exception(f"Directory {data_path} does not exist")

        # Establishing the connection
        conn = psycopg2.connect(
            database=self._db_name, user=self._db_user,
            password=self._db_password, host=self._db_host, port=self._db_port)
        conn.autocommit = True

        # Delete all rows from tables (in the opposite order)
        cursor = conn.cursor()
        for table in reversed(TABLES):
            cursor.execute(f"DELETE FROM {table}")

        # Populate tables
        for table in TABLES:
            # Check if <data_dir>/<table>.csv file exists
            if not os.path.isfile(f"{data_path}/{table}.csv"):
                raise Exception(f"File {data_path}/{table}.csv does not exist")

            # Populate table
            csv_file = open(f"{data_path}/{table}.csv", 'r')
            cursor.copy_from(csv_file, table, sep='|', null='')
            csv_file.close()

        cursor.close()
        conn.close()

        logging.debug("DATABASE: Table populated")

    def create_indexes(self, indexes_path: str = "./libs/tpch/schema/tpch_indexes.sql") -> None:
        """Create indexes.

        Args:
            indexes_path (str): Path of the indexes file. Default: "./libs/tpch/schema/tpch_indexes.sql"
        """
        # Establishing the connection
        conn = psycopg2.connect(
            database=self._db_name, user=self._db_user,
            password=self._db_password, host=self._db_host, port=self._db_port)
        conn.autocommit = True

        # Create indexes
        cursor = conn.cursor()
        with open(indexes_path) as f:
            dbgen = f.read()
            f.close()
        cursor.execute(dbgen)

        cursor.close()
        conn.close()

        logging.debug("DATABASE: Indexes created")
