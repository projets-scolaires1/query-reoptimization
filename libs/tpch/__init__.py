"""Python module to manage TPC-H Benchmark.

Classes:
    TPCH: Class to manage TPC-H Benchmark.
    CreateDatabase: Class to create TPC-H Benchmark database on PostgreSQL.
    RunQueries: Class to run TPC-H Benchmark queries on PostgreSQL.
"""

# Import classes
from .tpch import TPCH
from .create_database import CreateDatabase
from .run_queries import RunQueries
