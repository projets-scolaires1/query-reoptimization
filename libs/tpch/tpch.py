import os
import random
import re
import shutil
import logging
import subprocess


class TPCH:
    """Class to manage TPC-H Benchmark.

    Methods:
        _generate_makefile: Generate the TPC-H Benchmark Makefile.
        _install: Compile the TPC-H Benchmark tool.
        generate_database: Generate the database.
        generate_queries: Generate the queries.
        _qgen: Generate a TPC-H query.
        _dbgen: Generate the TPC-H data for the database.

    Attributes:
        _tpch_path (str): The path to the TPC-H Benchmark tool.
        _dbgen_path (str): The path to the dbgen directory.

    Usage:
        from libs.tpch import TPCH

        tpch = TPCH(tpch_path, output_path, cc, database, machine, workload)
        tpch.generate_database(scale, data_path)
        tpch.generate_queries(nb_queries, queries_path)
    """

    def __init__(self, tpch_path: str = "./tpch-tool", cc: str = "gcc", database: str = "POSTGRESQL", machine: str = "LINUX", workload: str = "TPCH") -> None:
        """Initialize the class.

        Args:
            tpch_path (str, optional): The path to the TPC-H Benchmark tool. Defaults to "./tpch-tool".
            cc (str, optional): The compiler to use. Defaults to "gcc".
            database (str, optional): The database to use. Defaults to "POSTGRESQL".
            machine (str, optional): The machine to use. Defaults to "LINUX".
            workload (str, optional): The workload to use. Defaults to "TPCH".
        """
        self._tpch_path = tpch_path
        self._dbgen_path = f"{self._tpch_path}/dbgen"

        # Configure TPCH
        self._generate_makefile(cc, database, machine, workload)
        self._install()

    def _generate_makefile(self, cc: str = "gcc", database: str = "POSTGRESQL", machine: str = "LINUX", workload: str = "TPCH") -> None:
        """Generate the TPC-H Benchmark Makefile.

        Args:
            cc (str, optional): The compiler to use. Defaults to "gcc".
            database (str, optional): The database to use. Defaults to "POSTGRESQL".
            machine (str, optional): The machine to use. Defaults to "LINUX".
            workload (str, optional): The workload to use. Defaults to "TPCH".
        """
        with open(f"{self._dbgen_path}/makefile.suite", "r") as makefile:
            makefile_content = makefile.read()
            makefile_content = re.sub(
                r"^CC\s*=(.*)$", f"CC = {cc}", makefile_content, flags=re.MULTILINE)
            makefile_content = re.sub(
                r"^DATABASE\s*=(.*)$", f"DATABASE = {database}", makefile_content, flags=re.MULTILINE)
            makefile_content = re.sub(
                r"^MACHINE\s*=(.*)$", f"MACHINE = {machine}", makefile_content, flags=re.MULTILINE)
            makefile_content = re.sub(
                r"^WORKLOAD\s*=(.*)$", f"WORKLOAD = {workload}", makefile_content, flags=re.MULTILINE)

        # Write the new Makefile
        with open(f"{self._dbgen_path}/Makefile", "w") as makefile:
            makefile.write(makefile_content)

    def _install(self) -> None:
        """Compile the TPC-H Benchmark tool."""
        os.system(f"cd {self._dbgen_path} && make")

    def generate_database(self, scale: float = 1, data_path: str = "./output/data") -> None:
        """Generate the database.

        Args:
            scale (float, optional): The size of the database in GB. Defaults to 1.
            data_path (float, optional): Path to store the CSV file that contains data.
        """
        logging.debug("TPC-H data generation in progress...")

        # Create the data directory if it does not exist
        os.makedirs(data_path, exist_ok=True)

        # Generate the data
        self._dbgen(scale, data_path)

        # Transform the .tbl files to .csv files
        for file in os.listdir(data_path):
            if os.path.isfile(os.path.join(data_path, file)) and file.endswith(".tbl"):
                # Remove the '|' character at the end of each line
                os.system(f"sed -i 's/|$//' {data_path}/{file}")

                # Move the file to the data directory and change the extension to .csv
                shutil.move(
                    f"{data_path}/{file}", f"{data_path}/{file.split('.')[0]}.csv")

        logging.debug("TPC-H data generation done")

    def generate_queries(self, nb_queries: int = 22, queries_path: str = "./output/queries_22") -> None:
        """Generate the queries.

        Args:
            nb_queries (int, optional): The number of queries to generate (e.g. 22 queries is one query per type). Defaults to 22.
            queries_path (float, optional): Path to store the queries. Defaults to "./output/queries_22".
        """
        logging.debug("Generate TPC-H queries...")

        # Create the queries directory and clear it if it already exists
        if os.path.exists(queries_path):
            shutil.rmtree(queries_path)
        os.makedirs(queries_path, exist_ok=True)
        shutil.copyfile(f"{self._dbgen_path}/dists.dss",
                        f"dists.dss")  # Get dists.dss

        # Check if the number of queries is valid (must be a multiple of 22)
        if nb_queries % 22 != 0:
            logging.error("The number of queries must be a multiple of 22.")
            raise Exception("The number of queries must be a multiple of 22.")

        # Variables
        nb_groups = nb_queries // 22
        query_types = list(range(1, 23))

        for group_id in range(1, nb_groups + 1):
            # Create the query group directory and clear it if it already exists
            os.makedirs(f"{queries_path}/execution_{group_id}")

            # Shuffle the order of the 22 queries
            queries = query_types.copy()
            random.shuffle(queries)

            for query_type in queries:
                query_id = (group_id - 1) * 22 + queries.index(query_type) + 1
                self._qgen(
                    query_type, f"{queries_path}/execution_{group_id}/{query_type}-{query_id}.sql")

        # Remove the dists.dss file
        os.remove(f"dists.dss")

        logging.debug("TPC-H queries generation done")

    def _qgen(self, query_type: int, sql_path: str) -> None:
        """Generate a TPC-H query.

        Args:
            query_type (int): Query type.
            sql_path (str): Path of the output file.
        """
        try:
            output_file = open(sql_path, "w")
            subprocess.run(
                [f"{self._dbgen_path}/qgen", "-v", "-c", f"{query_type}"],
                env=dict(os.environ, DSS_QUERY=f"{self._dbgen_path}/queries"),
                stdout=output_file, check=True)
            logging.debug(f"Query Q{query_type} generated (path: {sql_path}).")
        except subprocess.CalledProcessError as e:
            logging.error(f"Query not generated. {e}")
            raise Exception(f"Query not generated. {e}")

    def _dbgen(self, scale: float = 1, data_path: str = "./output/data_1") -> None:
        """Generate the TPC-H data for the database.

        Args:
            scale (float, optional): The size of the database in GB. Defaults to 1.
            data_path (float, optional): Path to store the files generated.
        """
        try:
            # Copy the dists.dss file in the executable directory
            shutil.copyfile(
                f"{self._dbgen_path}/dists.dss", f"{data_path}/dists.dss")

            current_path = os.getcwd()
            subprocess.run(
                [f"{current_path}/{self._dbgen_path}/dbgen", "-s", f"{scale}"],
                cwd=data_path, check=True)
            logging.debug(
                f"TPC-H data generated for S{scale} (path: {data_path}).")
        except subprocess.CalledProcessError as e:
            logging.error(f"Data not generated. {e}")
            raise Exception(f"Data not generated. {e}")
        finally:
            # Remove the dists.dss file
            os.remove(f"{data_path}/dists.dss")
