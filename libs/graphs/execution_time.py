import logging
from matplotlib import pyplot as plt
import seaborn as sns

from .common import GraphCommon


class GraphExecutionTime:
    """Class to generate the graph for the execution time.

    Methods:
        graph_execution_time: Generate the graph for the execution time.
        graph_execution_time_parallel: Generate the graph for the execution time over connections.

    Attributes:
        None

    Usage:
        from libs.graphs import GraphExecutionTime

        GraphExecutionTime.graph_execution_time(results_path, algorithms, scale, nb_queries, nb_trials)
    """

    @staticmethod
    def graph_execution_time(results_path: str, algorithms: list, tests_type: list, scale: float, nb_queries: int, nb_trials: int, nb_connections: int):
        """Generate the graph for the execution time.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            tests_type: List of tests type as follows: [{'ref': 'serial', 'name': 'Serial'}, ...]
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.
            nb_connections: Number of connections.
        """
        # Graph data
        graph_data = GraphCommon.get_dataframe_from_average_by_query_type(
            results_path, algorithms, tests_type, scale, nb_queries, nb_trials)

        # Create the plots for each algorithm side by side using seaborn
        plt.figure(figsize=(12, 8))

        # Define a custom color palette
        custom_palette = sns.color_palette(
            "muted", len(algorithms) * len(tests_type))

        # Plot based on execution_time (in seconds)
        sns.barplot(
            x='query_type', y='execution_time', hue='algorithm',
            data=graph_data, palette=custom_palette)
        plt.xlabel('Query type')
        plt.ylabel('Average execution time (seconds)')
        if len(tests_type) == 1:
            title = f'Average execution time for each query type ({tests_type[0]["name"]}, database of {scale}GB, {int(nb_queries / 22)} queries per type, {nb_trials} trials)'
        else:
            title = f'Average execution time for each query type (database of {scale}GB, {int(nb_queries / 22)} queries per type, {nb_trials} trials)'
        if 'parallel' in [type['ref'] for type in tests_type]:
            title += f'{nb_connections} connections for parallel execution'

        plt.title(title)
        plt.xticks(rotation=45)
        plt.legend(title='Algorithms', loc='upper left')

        # Add horizontal lines for each y tick
        y_ticks = plt.gca().get_yticks()
        for y in y_ticks:
            plt.axhline(y=y, color='grey', linestyle='--', linewidth=0.5)

        plt.tight_layout()

        # Save the graph
        graph_path = f"{results_path}/graph_execution_time_s{scale}_q{nb_queries}_t{nb_trials}.png"
        plt.savefig(graph_path)
        logging.info(f"Graph for execution time saved to {graph_path}")

    @staticmethod
    def graph_execution_time_parallel(results_path: str, algorithms: list, scale: float, nb_queries: int, nb_trials: int, connections: list):
        """Generate the graph for the execution time over connections.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.
            connections: List of connections.
        """
        # Graph data
        graph_data = GraphCommon.get_dataframe_from_average_by_query_type_parallel(
            results_path, algorithms, connections, scale, nb_queries, nb_trials)

        # Create the plots for each algorithm side by side using seaborn
        plt.figure(figsize=(12, 8))

        # Define a custom color palette
        custom_palette = sns.color_palette(
            "muted", len(algorithms) * len(connections))

        # Plot based on execution_time (in seconds)
        sns.barplot(
            x='query_type', y='execution_time', hue='algorithm',
            data=graph_data, palette=custom_palette)
        plt.xlabel('Query type')
        plt.ylabel('Average execution time (seconds)')

        plt.title(
            f'Average execution time for each query type over connections (database of {scale}GB, {int(nb_queries / 22)} queries per type, {nb_trials} trials)')
        plt.xticks(rotation=45)
        plt.legend(title='Algorithms', loc='upper left')

        # Add horizontal lines for each y tick
        y_ticks = plt.gca().get_yticks()
        for y in y_ticks:
            plt.axhline(y=y, color='grey', linestyle='--', linewidth=0.5)

        plt.tight_layout()

        # Save the graph
        graph_path = f"{results_path}/graph_execution_time_over_connections_s{scale}_q{nb_queries}_t{nb_trials}.png"
        plt.savefig(graph_path)
        logging.info(
            f"Graph for execution time over connections saved to {graph_path}")
