import glob
import pandas as pd


class GraphCommon:
    """Class that contains common graph functions.

    Methods:
        get_dataframe_from_average_by_query_type: Get the DataFrame from the average_by_query_type.csv file for all specified algorithms.
        get_dataframe_from_average_by_query_type_parallel: Get the DataFrame from the average_by_query_type.csv file for all specified algorithms for parallel execution.
        get_sla_violation_from_algorithms: Get the DataFrame from the sla_violation.json file for all specified algorithms.
        get_sla_violation_from_algorithms_parallel: Get the DataFrame from the sla_violation.json file for all specified algorithms for parallel execution.

    Attributes:
        None

    Usage:
        from libs.graphs import GraphCommon

        GraphCommon.get_dataframe_from_average_by_query_type(results_path, algorithms, scale, nb_queries, nb_trials)
        GraphCommon.get_sla_violation_from_algorithms(results_path, algorithms, scale, nb_queries, nb_trials)
    """

    @staticmethod
    def get_dataframe_from_average_by_query_type(results_path: str, algorithms: list, tests_type: list, scale: float, nb_queries: int, nb_trials: int) -> pd.DataFrame:
        """Get the DataFrame from the average_by_query_type.csv file for all specified algorithms.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            tests_type: List of tests type as follows: [{'ref': 'serial', 'name': 'Serial'}, ...]
                ref: Reference of the tests type (the name in the project folder 'tests').
                name: Name of the tests type (used for labels and colors in the plots).
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.

        Returns:
            The DataFrame from the average_by_query_type.csv file for all specified algorithms.
        """
        # Variables
        column_names = ['query_type', 'execution_time', 'monetary_cost']
        graph_data = {}

        # Get the path to the CSV file for each algorithm like this:
        # > f"{results_path}/{algorithm_ref}_*_s{scale}_q{nb_queries}_t{nb_trials}/average_by_query_type.csv"
        # > * is the node domain name (e.g. chifflot-1) which is not known in advance so we use a wildcard
        for type in tests_type:
            for algorithm in algorithms:
                # Get the first CSV file found else raise an exception
                csv_file_pattern = f"{results_path}/{algorithm['ref']}_{type['ref']}_*_s{scale}_q{nb_queries}_t{nb_trials}/average_by_query_type.csv"
                csv_file = next(
                    iter([f for f in glob.glob(csv_file_pattern)]), None)

                graph_data[f"{algorithm['name']} ({type['name']})"] = pd.read_csv(
                    csv_file, names=column_names).iloc[1:]

        # Concatenate all data into a single DataFrame with an additional 'algorithm' column
        data_frame = pd.concat(
            [data.assign(algorithm=algorithm) for algorithm, data in graph_data.items()])

        # Convert results to numeric
        data_frame['monetary_cost'] = pd.to_numeric(
            data_frame['monetary_cost'], errors='coerce')
        data_frame['execution_time'] = pd.to_numeric(
            data_frame['execution_time'], errors='coerce')
        data_frame['execution_time'] = data_frame['execution_time'] / 1000

        return data_frame

    @staticmethod
    def get_dataframe_from_average_by_query_type_parallel(results_path: str, algorithms: list, connections: list, scale: float, nb_queries: int, nb_trials: int) -> pd.DataFrame:
        """Get the DataFrame from the average_by_query_type.csv file for all specified algorithms.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            connections: List of connections as follows: [1, 2, 4]
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.

        Returns:
            The DataFrame from the average_by_query_type.csv file for all specified algorithms.
        """
        # Variables
        column_names = ['query_type', 'execution_time', 'monetary_cost']
        graph_data = {}

        # Get the path to the CSV file for each algorithm
        for conn in connections:
            for algorithm in algorithms:
                # Get the first CSV file found else raise an exception
                csv_file_pattern = f"{results_path}/{algorithm['ref']}_parallel_{conn}_*_s{scale}_q{nb_queries}_t{nb_trials}/average_by_query_type.csv"
                csv_file = next(
                    iter([f for f in glob.glob(csv_file_pattern)]), None)

                graph_data[f"{algorithm['name']} ({conn})"] = pd.read_csv(
                    csv_file, names=column_names).iloc[1:]

        # Concatenate all data into a single DataFrame with an additional 'algorithm' column
        data_frame = pd.concat(
            [data.assign(algorithm=algorithm) for algorithm, data in graph_data.items()])

        # Convert results to numeric
        data_frame['monetary_cost'] = pd.to_numeric(
            data_frame['monetary_cost'], errors='coerce')
        data_frame['execution_time'] = pd.to_numeric(
            data_frame['execution_time'], errors='coerce')
        data_frame['execution_time'] = data_frame['execution_time'] / 1000

        return data_frame

    @staticmethod
    def get_sla_violation_from_algorithms(results_path: str, algorithms: list, tests_type: list, scale: float, nb_queries: int, nb_trials: int) -> pd.DataFrame:
        """Get the DataFrame from the sla_violation.json file for all specified algorithms.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            tests_type: List of tests type as follows: [{'ref': 'serial', 'name': 'Serial'}, ...]
                ref: Reference of the tests type (the name in the project folder 'tests').
                name: Name of the tests type (used for labels and colors in the plots).
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.

        Returns:
            The DataFrame from the sla_violation.json file for all specified algorithms.
        """
        # Variables
        graph_data = {}

        # Get the path to the CSV file for each algorithm like this:
        # > f"{results_path}/{algorithm_ref}_*_s{scale}_q{nb_queries}_t{nb_trials}/sla_violation.json"
        # > * is the node domain name (e.g. chifflot-1) which is not known in advance so we use a wildcard
        for type in tests_type:
            for algorithm in algorithms:
                # Get the first CSV file found else raise an exception
                json_file_pattern = f"{results_path}/{algorithm['ref']}_{type['ref']}_*_s{scale}_q{nb_queries}_t{nb_trials}/sla_violation.json"
                json_file = next(
                    iter([f for f in glob.glob(json_file_pattern)]), None)

                # File content example: [{"nb_sla_violations":13.3333333333,"sla_violation_rate":60.6060606061}]
                with open(json_file, 'r') as file:
                    graph_data[f"{algorithm['name']} ({type['name']})"] = pd.read_json(
                        file)

        # Transform the dictionary into a DataFrame: algorithm | sla_violation_rate
        data_frame = pd.concat(
            [data.assign(algorithm=algorithm) for algorithm, data in graph_data.items()])
        data_frame = data_frame[['algorithm', 'sla_violation_rate']]

        return data_frame

    @staticmethod
    def get_sla_violation_from_algorithms_parallel(results_path: str, algorithms: list, connections: list, scale: float, nb_queries: int, nb_trials: int) -> pd.DataFrame:
        """Get the DataFrame from the sla_violation.json file for all specified algorithms for parallel execution.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            connections: List of connections as follows: [1, 2, 4]
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.

        Returns:
            The DataFrame from the sla_violation.json file for all specified algorithms.
        """
        # Variables
        graph_data = {}

        # Get the path to the CSV file for each algorithm
        for conn in connections:
            for algorithm in algorithms:
                # Get the first CSV file found else raise an exception
                json_file_pattern = f"{results_path}/{algorithm['ref']}_parallel_{conn}_*_s{scale}_q{nb_queries}_t{nb_trials}/sla_violation.json"
                json_file = next(
                    iter([f for f in glob.glob(json_file_pattern)]), None)

                # File content example: [{"nb_sla_violations":13.3333333333,"sla_violation_rate":60.6060606061}]
                with open(json_file, 'r') as file:
                    graph_data[f"{algorithm['name']} ({conn})"] = pd.read_json(
                        file)

        # Transform the dictionary into a DataFrame: algorithm | sla_violation_rate
        data_frame = pd.concat(
            [data.assign(algorithm=algorithm) for algorithm, data in graph_data.items()])
        data_frame = data_frame[['algorithm', 'sla_violation_rate']]

        return data_frame
