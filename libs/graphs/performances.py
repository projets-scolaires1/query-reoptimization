import glob
import logging
from matplotlib import pyplot as plt
import pandas as pd
import textwrap as twp
from plottable import Table

from .common import GraphCommon


class GraphPerformances:
    """Class to generate the table for performances.

    Methods:
        perf_results: Generate the table for performances.
        perf_results_parallel: Generate the table for performances over connections.

    Attributes:
        None

    Usage:
        from libs.graphs import GraphPerformances

        GraphPerformances.perf_results(results_path, algorithms, scale, nb_queries, nb_trials)
    """

    @staticmethod
    def perf_results(results_path: str, algorithms: list, tests_type: list, scale: float, nb_queries: int, nb_trials: int):
        """Generate the graph for the SLA violation.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            tests_type: List of tests type as follows: [{'ref': 'serial', 'name': 'Serial'}, ...]
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.

        Table example:

            | Algorithm  | Avg query Time (s) | Avg query cost ($) | SLA violation rate (%) |
            |------------|--------------------|--------------------|------------------------|
            | NoReOpt    | 11.42±8.6(1)       | 1.37±1.0(1)        | 43.06% (2)             |
            | SLAReOptRL | 12.31±6.1(2)       | 1.4±0.7(2)         | 33.14% (1)             |

            Explanation: Average values ± Standard deviation (Ranking)
        """
        # Variables
        std_dev_data = {}

        for type in tests_type:
            for algorithm in algorithms:
                # Get the first CSV file found else raise an exception
                json_file = f"{results_path}/{algorithm['ref']}_{type['ref']}_*_s{scale}_q{nb_queries}_t{nb_trials}/performances.json"
                json_file = next(iter([f for f in glob.glob(json_file)]), None)

                if json_file is None:
                    logging.error(
                        f"Unable to find the performances.json file for algorithm {algorithm['ref']}_{type['ref']}")
                    raise Exception(
                        f"Unable to find the performances.json file for algorithm {algorithm['ref']}_{type['ref']}")

                with open(json_file, 'r') as file:
                    std_dev_data[f"{algorithm['name']} ({type['name']})"] = pd.read_json(
                        file)

        # Transform the dictionary into a DataFrame: algorithm | avg_query_execution_time | avg_query_monetary_cost | avg_std_deviation_execution_time | avg_std_deviation_monetary_cost
        data_frame = pd.concat(
            [data.assign(algorithm=algorithm) for algorithm, data in std_dev_data.items()])
        data_frame = data_frame[[
            'algorithm', 'avg_query_execution_time', 'avg_query_monetary_cost',
            'avg_std_deviation_execution_time', 'avg_std_deviation_monetary_cost',
            'avg_trial_execution_time', 'nb_connections']]

        # Get SLA violation rate
        sla_violation_data = GraphCommon.get_sla_violation_from_algorithms(
            results_path, algorithms, tests_type, scale, nb_queries, nb_trials)

        # Merge the DataFrames
        data_frame = pd.merge(data_frame, sla_violation_data, on='algorithm')

        # Transform execution time in seconds
        data_frame['avg_query_execution_time'] = data_frame['avg_query_execution_time'] / 1000
        data_frame['avg_std_deviation_execution_time'] = data_frame['avg_std_deviation_execution_time'] / 1000
        data_frame['avg_trial_execution_time'] = data_frame['avg_trial_execution_time'] / 1000

        # Create DataFrame for the table
        columns = [
            # Algorithm name
            'Algorithm',
            # Average query execution time (s) ± Standard deviation for execution time (Ranking)
            'Avg query execution time (s)',
            # Average query monetary cost ($) ± Standard deviation for monetary cost (Ranking)
            'Avg query monetary cost ($)',
            # SLA violation rate (%)
            'SLA violation rate (%)',
            # Average execution time for a trial (s) (Ranking)
            'Avg execution time for a trial (s)',
            # Nb connections
            'Nb connections'
        ]

        table_data = []

        for _, row in data_frame.iterrows():
            execution_time_ranking = data_frame[
                data_frame['avg_query_execution_time'] < row['avg_query_execution_time']].shape[0] + 1
            monetary_cost_ranking = data_frame[
                data_frame['avg_query_monetary_cost'] < row['avg_query_monetary_cost']].shape[0] + 1
            sla_violation_ranking = data_frame[
                data_frame['sla_violation_rate'] < row['sla_violation_rate']].shape[0] + 1
            avg_execution_time_ranking = data_frame[
                data_frame['avg_trial_execution_time'] < row['avg_trial_execution_time']].shape[0] + 1

            table_data.append([
                row['algorithm'],
                f"{row['avg_query_execution_time']:.2f}±{row['avg_std_deviation_execution_time']:.2f} ({execution_time_ranking})",
                f"{row['avg_query_monetary_cost']:.2f}±{row['avg_std_deviation_monetary_cost']:.2f} ({monetary_cost_ranking})",
                f"{row['sla_violation_rate']:.2f}% ({sla_violation_ranking})",
                f"{row['avg_trial_execution_time']:.2f} ({avg_execution_time_ranking})",
                row['nb_connections']
            ])

        # dataframe with data and columns specified
        df = pd.DataFrame(table_data, columns=columns)

        # Add twd of 25 characters for all cells
        factor = 18
        df.columns = df.columns.map(lambda x: twp.fill(x, factor))
        df.iloc[:, 0] = df.iloc[:, 0].map(lambda x: twp.fill(x, factor))

        # d = pd.DataFrame(np.random.random((5, 5)), columns=["A", "B", "C", "D", "E"]).round(2)
        plt.subplots(figsize=(18, 8))
        plt.axis('off')
        plt.axis('tight')
        Table(df, index_col='Algorithm',
              even_row_color='lightgrey', row_dividers=False)

        # Save the table
        table_path = f"{results_path}/table_performances_results_s{scale}_q{nb_queries}_t{nb_trials}.png"
        plt.savefig(table_path, dpi=300, transparent=True, bbox_inches='tight')

        logging.info(f"Table of performances saved to {table_path}")

    @staticmethod
    def perf_results_parallel(results_path: str, algorithms: list, scale: float, nb_queries: int, nb_trials: int, connections: list):
        """Generate the graph for the SLA violation over connections.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.
            connections: List of connections.

        Table example:

            | Algorithm  | Avg query Time (s) | Avg query cost ($) | SLA violation rate (%) |
            |------------|--------------------|--------------------|------------------------|
            | NoReOpt    | 11.42±8.6(1)       | 1.37±1.0(1)        | 43.06% (2)             |
            | SLAReOptRL | 12.31±6.1(2)       | 1.4±0.7(2)         | 33.14% (1)             |

            Explanation: Average values ± Standard deviation (Ranking)
        """
        # Variables
        std_dev_data = {}

        for conn in connections:
            for algorithm in algorithms:
                # Get the first CSV file found else raise an exception
                json_file = f"{results_path}/{algorithm['ref']}_parallel_{conn}_*_s{scale}_q{nb_queries}_t{nb_trials}/performances.json"
                json_file = next(iter([f for f in glob.glob(json_file)]), None)

                if json_file is None:
                    logging.error(
                        f"Unable to find the performances.json file for algorithm {algorithm['ref']}_parallel_{conn}")
                    raise Exception(
                        f"Unable to find the performances.json file for algorithm {algorithm['ref']}_parallel_{conn}")

                with open(json_file, 'r') as file:
                    std_dev_data[f"{algorithm['name']} ({conn})"] = pd.read_json(
                        file)

        # Transform the dictionary into a DataFrame: algorithm | avg_query_execution_time | avg_query_monetary_cost | avg_std_deviation_execution_time | avg_std_deviation_monetary_cost
        data_frame = pd.concat(
            [data.assign(algorithm=algorithm) for algorithm, data in std_dev_data.items()])
        data_frame = data_frame[[
            'algorithm', 'avg_query_execution_time', 'avg_query_monetary_cost',
            'avg_std_deviation_execution_time', 'avg_std_deviation_monetary_cost',
            'avg_trial_execution_time', 'nb_connections']]

        # Get SLA violation rate
        sla_violation_data = GraphCommon.get_sla_violation_from_algorithms_parallel(
            results_path, algorithms, connections, scale, nb_queries, nb_trials)

        # Merge the DataFrames
        data_frame = pd.merge(data_frame, sla_violation_data, on='algorithm')

        # Transform execution time in seconds
        data_frame['avg_query_execution_time'] = data_frame['avg_query_execution_time'] / 1000
        data_frame['avg_std_deviation_execution_time'] = data_frame['avg_std_deviation_execution_time'] / 1000
        data_frame['avg_trial_execution_time'] = data_frame['avg_trial_execution_time'] / 1000

        # Create DataFrame for the table
        columns = [
            # Algorithm name
            'Algorithm',
            # Average query execution time (s) ± Standard deviation for execution time (Ranking)
            'Avg query execution time (s)',
            # Average query monetary cost ($) ± Standard deviation for monetary cost (Ranking)
            'Avg query monetary cost ($)',
            # SLA violation rate (%)
            'SLA violation rate (%)',
            # Average execution time for a trial (s) (Ranking)
            'Avg execution time for a trial (s)',
            # Nb connections
            'Nb connections'
        ]

        table_data = []

        for _, row in data_frame.iterrows():
            execution_time_ranking = data_frame[
                data_frame['avg_query_execution_time'] < row['avg_query_execution_time']].shape[0] + 1
            monetary_cost_ranking = data_frame[
                data_frame['avg_query_monetary_cost'] < row['avg_query_monetary_cost']].shape[0] + 1
            sla_violation_ranking = data_frame[
                data_frame['sla_violation_rate'] < row['sla_violation_rate']].shape[0] + 1
            avg_execution_time_ranking = data_frame[
                data_frame['avg_trial_execution_time'] < row['avg_trial_execution_time']].shape[0] + 1

            table_data.append([
                row['algorithm'],
                f"{row['avg_query_execution_time']:.2f}±{row['avg_std_deviation_execution_time']:.2f} ({execution_time_ranking})",
                f"{row['avg_query_monetary_cost']:.2f}±{row['avg_std_deviation_monetary_cost']:.2f} ({monetary_cost_ranking})",
                f"{row['sla_violation_rate']:.2f}% ({sla_violation_ranking})",
                f"{row['avg_trial_execution_time']:.2f} ({avg_execution_time_ranking})",
                row['nb_connections']
            ])

        # dataframe with data and columns specified
        df = pd.DataFrame(table_data, columns=columns)

        # Add twd of 25 characters for all cells
        factor = 18
        df.columns = df.columns.map(lambda x: twp.fill(x, factor))
        df.iloc[:, 0] = df.iloc[:, 0].map(lambda x: twp.fill(x, factor))

        # d = pd.DataFrame(np.random.random((5, 5)), columns=["A", "B", "C", "D", "E"]).round(2)
        plt.subplots(figsize=(18, 8))
        plt.axis('off')
        plt.axis('tight')
        Table(df, index_col='Algorithm',
              even_row_color='lightgrey', row_dividers=False)

        # Save the table
        table_path = f"{results_path}/table_performances_results_over_connections_s{scale}_q{nb_queries}_t{nb_trials}.png"
        plt.savefig(table_path, dpi=300, transparent=True, bbox_inches='tight')

        logging.info(
            f"Table of performances saved over connections to {table_path}")
