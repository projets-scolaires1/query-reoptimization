import logging
from matplotlib import pyplot as plt
import seaborn as sns

from .common import GraphCommon


class GraphSLAViolation:
    """Class to generate the graph for the SLA violation.

    Methods:
        sla_violation: Generate the graph for the SLA violation.
        sla_violation_parallel: Generate the graph for the SLA violation over connections.

    Attributes:
        None

    Usage:
        from libs.graphs import GraphSLAViolation

        GraphSLAViolation.sla_violation(results_path, algorithms, scale, nb_queries, nb_trials)
    """

    @staticmethod
    def sla_violation(results_path: str, algorithms: list, tests_type: list, scale: float, nb_queries: int, nb_trials: int, nb_connections: int):
        """Generate the graph for the SLA violation.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            tests_type: List of tests type as follows: [{'ref': 'serial', 'name': 'Serial'}, ...]
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.
            nb_connections: Number of connections.
        """
        # Graph data
        graph_data = GraphCommon.get_sla_violation_from_algorithms(
            results_path, algorithms, tests_type, scale, nb_queries, nb_trials)

        # Create the plots for each algorithm side by side using seaborn
        plt.figure(figsize=(10, 6))

        # Define a custom color palette
        custom_palette = sns.color_palette(
            "muted", len(algorithms) * len(tests_type))

        # Plot based on execution_time (in seconds)
        sns.barplot(
            x='algorithm', y='sla_violation_rate', hue='algorithm',
            data=graph_data, palette=custom_palette)
        plt.xlabel('Algorithm')
        plt.ylabel('SLA Violation Rate %')
        if len(tests_type) == 1:
            title = f'SLA Violation Rate by Algorithm ({tests_type[0]["name"]}, database of {scale}GB, {int(nb_queries / 22)} queries per type, {nb_trials} trials)'
        else:
            title = f'SLA Violation Rate by Algorithm (database of {scale}GB, {int(nb_queries / 22)} queries per type, {nb_trials} trials)'
        if 'parallel' in [type['ref'] for type in tests_type]:
            title += f'{nb_connections} connections for parallel execution'

        plt.title(title)
        plt.xticks(rotation=45, ha="right")

        plt.tight_layout()

        # Save the graph
        graph_path = f"{results_path}/graph_sla_violation_s{scale}_q{nb_queries}_t{nb_trials}.png"
        plt.savefig(graph_path)
        logging.info(f"Graph for SLA violation saved to {graph_path}")

    @staticmethod
    def sla_violation_parallel(results_path: str, algorithms: list, scale: float, nb_queries: int, nb_trials: int, connections: list):
        """Generate the graph for the SLA violation over connections.

        Args:
            results_path: Path to the results.
            algorithms: List of algorithms as follows: [{'ref': 'no_reoptimization', 'name': 'NoReOpt'}, ...]
                ref: Reference of the algorithm (the name in the project folder 'algorithms').
                name: Name of the algorithm (used for labels and colors in the plots).
            scale: Scale factor.
            nb_queries: Number of queries.
            nb_trials: Number of trials.
            connections: List of connections.
        """
        # Graph data
        graph_data = GraphCommon.get_sla_violation_from_algorithms_parallel(
            results_path, algorithms, connections, scale, nb_queries, nb_trials)

        # Create the plots for each algorithm side by side using seaborn
        plt.figure(figsize=(10, 6))

        # Define a custom color palette
        custom_palette = sns.color_palette(
            "muted", len(algorithms) * len(connections))

        # Plot based on execution_time (in seconds)
        sns.barplot(
            x='algorithm', y='sla_violation_rate', hue='algorithm',
            data=graph_data, palette=custom_palette)
        plt.xlabel('Algorithm')
        plt.ylabel('SLA Violation Rate %')

        plt.title(
            f'SLA Violation Rate by Algorithm over connections (database of {scale}GB, {int(nb_queries / 22)} queries per type, {nb_trials} trials)')
        plt.xticks(rotation=45, ha="right")

        plt.tight_layout()

        # Save the graph
        graph_path = f"{results_path}/graph_sla_violation_over_connections_s{scale}_q{nb_queries}_t{nb_trials}.png"
        plt.savefig(graph_path)
        logging.info(
            f"Graph for SLA violation over connections saved to {graph_path}")
