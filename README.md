# Query ReOptimization project

## Overview

Project for research on query ReOptimization on cloud databases.

The three main optimization goals, of this project, are:

- Minimize the query execution time;
- Minimize the query monetary costs;
- And minimize the query SLA (Service Level Agreement) violations.

In cloud databases, the cloud provider needs to provide a Service Level Agreement (SLA) to the user, which is a contract that defines the quality of service that the user will receive. The SLA is signed between the users and the cloud provider. Finally, the cloud provider needs to respect the execution time and the monetary cost defined in the SLA. If the cloud provider does not respect the SLA, the user can receive a discount or even a refund.

### Structure

The project is organized as follows:

- ***.vscode/***: This folder contains the project settings for Visual Studio Code (it is a development environment);
- ***algorithms/***: This folder contains the PostgreSQL version with the implemented algorithms;
- ***libs/***: This folder contains the Python libraries. These libraries enable modularity;
- ***tests/***: This folder contains the data sets and the results for the test base execution;
- ***tpch-tool/***: This folder contains the TPC-H Benchmark tools for tests (e.g. see TPC-H Benchmark). This is the folder used to generate the commands used to generate data and queries;
- ***.gitignore***: This file is used to tell Git to ignore updates to certain files;
- ***.gitlab-ci.yml***: This file is the configuration file for Gitlab CI (automation tool for continuous integration, deployment and delivery);
- ***g5k_execution.py***: This Python script run tests on the Grid'5000 platform, to execute the *setup.py* and *run.py* scripts;
- ***generate_data.py***: This Python script is used to generate data or queries with the TPC-H Benchmark tool;
- ***graphs_parallel.py***: This Python script does the same thing as *graphs.py*, but for parallel execution tests;
- ***graphs.py***: This Python script post-processes the results and generates graphs;
- ***README.md***: This is the file containing all the documentation for the Git project (it is this file);
- ***requirements.txt***: This file lists the Python dependencies to be installed (using the *pip* command);
- ***run.py***: This Python script execute queries on the installed PostgreSQL instance (also get execution times and monetary costs);
- ***setup.py***: This Python script install PostgreSQL and create the database;

### Built With

This project is based on the following technologies:

- **Bash/Shell:** It is a script command-line interpreter for use under a Linux command terminal
- **Git:** It is a source code version management software. (To learn how to use it: [https://www.cristal.univ-lille.fr/TPGIT/](https://www.cristal.univ-lille.fr/TPGIT/));
- **Gitlab:** It is an online Git repository management software, including wiki (documentation) and ticket system (tasks management);
- **Gitlab CI:** It is a software development tool for automating processes or tasks in the software lifecycle;
- **Grid’5000:** It is a large-scale and flexible testbed infrastructure;
- **PostgreSQL:** It is an open source, SQL-compliant relational database management system (RDBMS);
- **Python:** It is an object-oriented programming language;
- **TPC-H Benchmark:** It is a series of standardized tests evaluating the performance of DBMS.

## Getting Started

### Prerequisites

To use this project the following is needed:

- A personal computer with a Linux operating system (when we say "**local**", we mean a personal environment on your own computer). The project were tested on Debian GNU/Linux 11 (bullseye);
- A Grid’5000 platform access;

#### Request access to Grid'5000

To request a Grid'5000 account, it is necessary to make a request via the form: [https://www.grid5000.fr/w/Grid5000:Get_an_account](https://www.grid5000.fr/w/Grid5000:Get_an_account)

In the request specify membership of IRISA's SHAMAN team, under the supervision of Laurent D'Orazio.

### Installation (on the personal computer)

First you need to install the prerequisites:

```bash
sudo apt update
sudo apt install build-essential libssl-dev libffi-dev python3-dev python3-pip virtualenv python3-virtualenv
```

Next, you need to retrieve the project using Git or Google Drive.

1. Git
    
    Execute the following commands:
    
    ```bash
    git clone https://gitlab.papierpain.fr/enssat/query-reoptimization-project.git
    cd query-reoptimization-project
    ```
    
2. Google Drive
    
    Go to the following link: [https://drive.google.com/drive/folders/1z2NztXnC0apQuhUN8YxDaJmxPMrv_0C0?usp=drive_link](https://drive.google.com/drive/folders/1z2NztXnC0apQuhUN8YxDaJmxPMrv_0C0?usp=drive_link)
    
    Then, download this file: Steven/query-reoptimization-project-master.zip
    
    Finally, extract the archive and open a terminal inside for the new steps.
    

To finalize the installation, run the following commands in the downloaded project folder:

```bash
virtualenv -p python3 .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```

Now, you are ready to execute all Python scripts.

#### Grid'5000 connection (SSH access)

To access to the resources on Grid’5000, be sure to have your `~/.ssh/config` file configured as following:

```bash
Host g5k
  User <username>
  Hostname access.grid5000.fr
  ForwardAgent no
  IdentityFile ~/.ssh/grid5000_key

Host *.g5k
  User <username>
  ProxyCommand ssh g5k -W "$(basename %h .g5k):%p"
  ForwardAgent no
  IdentityFile ~/.ssh/grid5000_key
```

Be sure to replace your username and check that the path to your private key is correct (the one you provided when you created your account).

You can find more information on the Grid’5000 website: [https://www.grid5000.fr/w/SSH#Using_SSH_ProxyCommand_feature_to_ease_the_access_to_hosts_inside_Grid.275000](https://www.grid5000.fr/w/SSH#Using_SSH_ProxyCommand_feature_to_ease_the_access_to_hosts_inside_Grid.275000)

## Example (tutorial)

The aim of this tutorial is to show you how to run Grid'5000. To do this, we'll run two tests, the first of which we'll describe step by step.

- Both tests will have the same query set, which is 22 queries generated with the TPC-H Benchmark tool (query set available in *tests/data_sets/queries_22*);
- The dataset will be run three times, the aim being to reduce the impact of random inconsistencies at runtime;
- Next, the algorithm used will be the PostgreSQL default, called here "**no_reoptimization**" (accessible in *algorithms/no_reoptimization*);
- Finally, the only difference between the two tests is the size of the database, which is 100 MB for one and 1GB for the other (TPC-H scale factor of 0.1 and 1).

### Grid’5000 node selection

The Grid’5000 platform is distributed over 8 sites (in France), enabling researchers to reserve clusters (part of sites) or nodes (part of clusters) for their experiments.

For our experiments, we chose a “**chifflot**” cluster node on the **Lille** site. The characteristics of the nodes in this cluster are as follows:

- 2 CPUs Intel Xeon Gold 6126;
- 12 core/CPU;
- 2 GPUs Tesla P100-PCIE-17GB;
- 192GB RAM;
- 2x447GB SSD;
- 2 x 25Gb Ethernet.

To choose your node, go to the following page and select an available node for the next 45 minutes: [https://intranet.grid5000.fr/oar/Lille/drawgantt-svg/?filter=chifflot only](https://intranet.grid5000.fr/oar/Lille/drawgantt-svg/?filter=chifflot%20only)

For the next explanation the node “**chifflot-2**” is selected.

### Prepare the query set

To use an existing query set, you need to generate it in your personal computer if you haven't already done so, and then upload it to the server (on the Grid’5000 site). In our case, the queries already exist in the *tests/data_sets/queries_22* folder, so we'll just copy them with the following command:

```bash
# Create the folder for the queries
ssh lille.g5k
mkdir generated_queries
exit
# Copy the queries
scp -r tests/data_sets/queries_22 lille.g5k:~/generated_queries/
```

### Execute tests on Grid’5000

To execute the test on Grid'5000, run the following command (**be sure to replace the values when the pattern <xxx> exist**):

```bash
python ./g5k_execution.py \
  -u <username> -p <path-to-your-g5k-ssh-private-key> -s lille -w "00:45" \
  -o /usr/local/pgsql -a no_reoptimization -S 0.1 -q 22 -t 3 \
  -U https://gitlab.papierpain.fr/enssat/query-reoptimization-project \
  -b master -n <the-available-node-you-want>
```

### Check test progression

To check the progression of the test, you can:

- Go to the [Grid'5000 website](https://intranet.grid5000.fr/oar/Lille/drawgantt-svg/) and search for the node you reserved (if it's 'terminated', the test is finished);
- Or connect to the site (e.g. lille), with the following commands:
    
    ```bash
    ssh lille.g5k
    oarstat # To check the status of all reserved nodes (for the site)
    cat OAR.1962412.stderr # To check the output of the test (the number 1962412 is the job id)
    ```
    

After the test is finished, you can retrieve the results on the site (by ssh) in the folder *~/results*.

### Generate graphs

To get results on your personal computer and generate the graphs execute the following commands:

```bash
python ./graphs.py \
  -u <g5k-username> -p <path-to-your-g5k-ssh-private-key> \
  -a no_reoptimization -S 0.1 -q 22 -t 3 -s lille
```

You need to retrieve the results/graphs in the folder `output/results` and you can verify the results with the results/graphs in the folder `tests/results`.

### Results and graphs explanations

The structure is the following:

```bash
tests/results
├── graph_execution_time_s0.1_q22_t3.png
├── graph_monetary_cost_s0.1_q22_t3.png
├── graph_sla_violation_s0.1_q22_t3.png
├── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
│   ├── average_by_query_type.csv
│   ├── execution_times.json
│   ├── performances.json
│   ├── sla_violation.json
│   ├── trial_1
│   │   └── output_execution_1.csv
│   ├── trial_2
│   └── trial_3
└── table_performances_results_s0.1_q22_t3.png
```

#### Execution results

After the execution of the tests only the content of the folders **no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3/trial_<x>** are generated. They contain the CSV files, with the results (execution time and monetary cost) for each execution (each query). There is also the file **execution_times.json** that contain the execution time to execute each trial (one trial is one execution of all the query set).

#### Generated results (post-treatments)

The other files in **no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3** are generated for:

- **average_by_query_type.csv**: Average execution time and monetary cost per query type;
- **performances.json**: Contain the following information:
    - Average execution time for a query;
    - Average monetary cost for a query;
    - Average standard deviation for the execution time;
    - Average standard deviation for the monetary cost;
    - Average execution time for a trial (to execution all the query set one time);
    - Number of connection (concern parallel execution, so here it is one connection for serial execution).
- **sla_violation.json**: SLA violation rate and the number of violation.

#### Graphs and table

The graphs and the table are structured as follow:

```bash
tests/results/<type>_<title>_s<database_size>_q<number_of_queries>_t<number_of_trials>.<extension>
```

There are three graphs and one table:

- **graph_execution_time**: Graph showing average execution time per query type for all algorithms (here only "*no_reoptimization*");
- **graph_monetary_cost**: Graph showing average monetary cost per query type for all algorithms (here only "*no_reoptimization*");
- **graph_sla_violation**: Graph showing SLA violation rate for all algorithms (here only "*no_reoptimization*");
- **table_performances_results**: Table that resume the performances (execution time, monetary cost, sla violation and execution time for the entire query set) for all algorithms (here only "*no_reoptimization*");
    
    Columns 2 to 5 are structured as follows: Average values +- Standard deviation (ranking)
    

### The second test

Now you can execute the same process for a 1GB database (so a scale of 1).

So, on your personal computer:

```bash
python ./g5k_execution.py \
  -u <username> -p <path-to-your-g5k-ssh-private-key> -s lille -w "00:45" \
  -o /usr/local/pgsql -a no_reoptimization -S 1 -q 22 -t 3 \
  -U https://gitlab.papierpain.fr/enssat/query-reoptimization-project \
  -b master -n <the-available-node-you-want>

python ./graphs.py \
  -u <g5k-username> -p <path-to-your-g5k-ssh-private-key> \
  -a no_reoptimization -S 1 -q 22 -t 3 -s lille
```

## Usage

In all, there are six Python scripts, which we'll describe below. Note that the documentation below is the same as in the project files themselves. The files are structured as follows.

To run a complete test locally (on a personal computer or manually), you will need to execute the following two scripts in the order shown:

- `setup.py` to install and configure your environment;
- `run.py` to run a set of queries.

For test execution on the Grid'5000 platform, use the `g5k_execution.py` script.

To generate graphs, use one of the following two scripts:

- `graphs.py`;
- `graphs_parallel.py` to compare the different algorithms with parallel execution.

You can also generate data with the TPC-H Benchmark tool before testing, using the `generate_data.py` script.

### Script g5k_execution.py

This is a Python script for running a complete test on the Grid'5000 platform, which involves the following steps:

- Reserve a Grid'5000 node (see Grid'5000 documentation)
- Execute a bash script to:
    - Retrieve the project;
    - Retrieve any data sets;
    - Configure the environment ([setup.py](http://setup.py/) script);
    - Run the queries ([run.py](http://run.py/) script);
    - And retrieve the results in the *results/* folder on the Grid'5000 server.

#### Usage

```bash
g5k_execution.py -u <g5k_user> -p <g5k_pkey_path> -n <node> -s <site> [-d <disk>] [-w <walltime>] [-o <node_pg_data_path>] [-a <algorithm>] [-S <scale>] [-q <nb_queries>] [-t <nb_trials>] [-T <tests_type>] [-c <nb_connections>] [-U <git_project_url>] [-b <git_project_branch>] [-v]
```

#### Arguments

- `-u <g5k_user>`, `--g5k-user <g5k_user>`
    
    The Grid'5000 username.
    
- `-p <g5k_pkey_path>`, `--g5k-pkey-path <g5k_pkey_path>`
    
    The path to the Grid'5000 private key.
    
    Default: ~/.ssh/grid5000_key
    
- `-n <node>`, `--node <node>`
    
    The node to reserve (e.g. chifflot-1).
    
- `-s <site>`, `--site <site>`
    
    The site to use (e.g. lille).
    
- `-d <disk>`, `--disk <disk>`
    
    The disk to use (e.g. disk1).
    
    Default: disk1
    
- `-w <walltime>`, `--walltime <walltime>`
    
    The walltime for the node reservation.
    
    Default: 00:30
    
- `-o <node_pg_data_path>`, `--node-pg-data-path <node_pg_data_path>`
    
    The path to the PostgreSQL data directory on the node.
    
    Default: /usr/local/pgsql
    
- `-a <algorithm>`, `--algorithm <algorithm>`
    
    The algorithm to use.
    
    Default: no_reoptimization
    
- `-S <scale>`, `--scale <scale>`
    
    The scale factor to use.
    
    Default: 1
    
- `-q <nb_queries>`, `--nb-queries <nb_queries>`
    
    The number of queries to generate (e.g. 22 queries is one query per type).
    
    Default: 22
    
- `-t <nb_trials>`, `--nb-trials <nb_trials>`
    
    Number of trials.
    
    Default: 1
    
- `-T <tests_type>`, `--tests-type <tests_type>`
    
    The type of tests (queries execution) to run (e.g. serial or parallel).
    
    Default: serial
    
- `-c <nb_connections>`, `--nb-connections <nb_connections>`
    
    The number of connections to use for parallel execution tests (if serial execution, it is surcharged with the value 1).
    
    Default: 2
    
- `-U <git_project_url>`, `--git-project-url <git_project_url>`
    
    The Git project URL containing test programs and algorithms.
    
    Default: https://gitlab.papierpain.fr/enssat/query-reoptimization-project
    
- `-b <git_project_branch>`, `--git-project-branch <git_project_branch>`
    
    The Git project branch to use.
    
    Default: master
    
- `-v`, `--verbose`
    
    Increase output verbosity.
    

#### Example

```bash
python3 g5k_execution.py -u sgourves -p ~/.ssh/grid5000_key -n chifflot-1 -s lille -a querysplit -S 10 -q 220 -t 5
```

#### Output

1. Reservation failed
    
    If the reservation fails, check your settings and if they are correct, this means that the Grid'5000 node is already reserved by someone else on the platform.
    See the current reservations on this link (adapt the site name by your own with a capital letter on the first letter): [https://intranet.grid5000.fr/oar/Lille/drawgantt-svg](https://intranet.grid5000.fr/oar/Lille/drawgantt-svg)
    
2. Reservation ended successfully
    
    When your reservation is ended correctly, it means that on the reservations link, your node shows “terminated” state. If it’s the case, your results are on the server (in the chosen site, e.g. `ssh lille.g5k`) in the *results/* folder.
    
    The results are structured as follow:
    
    ```bash
    results
    └── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
        ├── execution_times.json # Contains the execution time of each trials (time between the start and the end of the trial)
        ├── trial_1 # Contains results of the first trial
        │   └── output_execution_1.csv # Contains execution time and monetary cost of each query of the query set
        ├── trial_2
        └── trial_3
    ```
    
    The results folder format is the following:
    
    ```bash
    results/<algorithm>_<tests_type>_<nb_connections>_<node_hostname>_s<scale>_q<nb_queries>_t<nb_trials>
    ```
    
3. Reservation ended in failure
    
    When it fails, it means that the time is up (insufficient **walltime**) or that the test has failed.
    
    To check the status and this logs, you can connect you on the Grid’5000 site with:
    
    ```bash
    ssh lille.g5k
    oarstat # To check the status of all reserved nodes (for the site)
    cat OAR.1962412.stderr # To check the error output of the test (the number 1962412 is the job id)
    cat OAR.1962412.stdout # To check the output of the test (the number 1962412 is the job id)
    ```
    

### Script generate_data.py

This Python script is used to generate database data (in CSV format) or a set of queries (SQL files) with TPC-H Benchmark tool.

#### Usage

```bash
generate_data.py [-p <path>] [-o <output>] [-c <compiler>] [-d <database>] [-m <machine>] [-w <workload>] [-s <scale>] [-q <nb_queries>] [-f] [-v]
```

#### Arguments

- `-p <path>`, `--path <path>`
    
    The path to the TPC-H Benchmark tool.
    
    Default: ./tpch-tool
    
- `-o <output>`, `--output <output>`
    
    The path to the output directory.
    
    Default: ./output
    
- `-c <compiler>`, `--compiler <compiler>`
    
    The compiler to use.
    
    Default: gcc
    
- `-d <database>`, `--database <database>`
    
    The database to use.
    
    Default: POSTGRESQL
    
- `-m <machine>`, `--machine <machine>`
    
    The machine to use.
    
    Default: LINUX
    
- `-w <workload>`, `--workload <workload>`
    
    The workload to use.
    
    Default: TPCH
    
- `-s <scale>`, `--scale <scale>`
    
    The scale factor to generate the database data.
    
- `-q <nb_queries>`, `--nb-queries <nb_queries>`
    
    The number of queries to generate (e.g. 22 queries is one query per type).
    
- `-f`, `--force`
    
    Force the generation of the data even if the output directories already exist.
    
- `-v`, `--verbose`
    
    Increase output verbosity.
    

#### Example

```bash
python3 generate_data.py -p ./tpch-tool -o ./output -s 1 -q 22
```

#### Output

Results depend on the options chosen. The results are located in the folder specified by the `--output` argument.

For queries, the format is: `<output>/queries_<nb_queries>`:

```bash
output/queries_22
└── execution_1
    ├── 10-12.sql
    ├── 11-18.sql
    ├── ...
    └── 9-9.sql
```

The command divides the number of queries by 22 and creates a folder of 22 queries for each group, in `execution_<i>` format. These folders then contain one SQL file per query type, in the format `<query_type>-<execution_order>.sql`.

For data, the format is: `<output>/data_<scale>`:

```bash
output/data_0.1
├── customer.csv
├── lineitem.csv
├── nation.csv
├── orders.csv
├── part.csv
├── partsupp.csv
├── region.csv
└── supplier.csv
```

The command generates a CSV file for each table in the database.

### Script graphs_parallel.py

This Python script is used to generate graphs for tests with parallel execution to compare algorithms or different numbers of parallel connections. It's based on the graphs.py script.

#### Usage

```bash
graphs_parallel.py -u <g5k_user> -p <g5k_pkey_path> -s <site> [-a <algorithms>] [-S <scale>] [-q <nb_queries>] [-t <nb_trials>] [-o <output>] [-c <connections>] [-l] [-v]
```

#### Arguments

- `-u <g5k_user>`, `--g5k-user <g5k_user>`
    
    The Grid'5000 username.
    
- `-p <g5k_pkey_path>`, `--g5k-pkey-path <g5k_pkey_path>`
    
    The path to the Grid'5000 private key.
    
    Default: ~/.ssh/grid5000_key
    
- `-s <site>`, `--site <site>`
    
    The Grid'5000 site.
    
- `-a <algorithms>`, `--algorithms <algorithms>`
    
    List of algorithms to use for the graphs (e.g. no_reoptimization, querysplit).
    
    Default: no_reoptimization
    
- `-S <scale>`, `--scale <scale>`
    
    The scale factor to use.
    
    Default: 1.0
    
- `-q <nb_queries>`, `--nb-queries <nb_queries>`
    
    The number of queries to generate (e.g. 22 queries is one query per type).
    
    Default: 22
    
- `-t <nb_trials>`, `--nb-trials <nb_trials>`
    
    Number of trials.
    
    Default: 1
    
- `-o <output>`, `--output <output>`
    
    The path to the output directory in local.
    
    Default: output/results
    
- `-c <connections>`, `--connections <connections>`
    
    List of connections to use for the graphs (e.g. 1,2,4,8).
    
    Default: 2
    
- `-l`, `--local`
    
    If the results are local or not.
    
- `-v`, `--verbose`
    
    Increase output verbosity.
    

#### Example

```bash
python3 graphs_parallel.py -a no_reoptimization,querysplit -S 10 -q 220 -t 3
```

#### Output

The results will be presented as follows:

```bash
output/results
├── graph_execution_time_over_connections_s10.0_q220_t3.png
├── graph_monetary_cost_over_connections_s10.0_q220_t3.png
├── graph_sla_violation_over_connections_s10.0_q220_t3.png
├── no_reoptimization_parallel_2_chifflot-3_s10.0_q220_t3
│   ├── average_by_query_type.csv
│   ├── execution_times.json
│   ├── performances.json
│   ├── sla_violation.json
│   ├── trial_1
│   ├── trial_2
│   └── trial_3
├── no_reoptimization_parallel_4_chifflot-3_s10.0_q220_t3
├── querysplit_parallel_2_chifflot-8_s10.0_q220_t3
├── querysplit_parallel_4_chifflot-8_s10.0_q220_t3
└── table_performances_results_over_connections_s10.0_q220_t3.png
```

1. Generated results (post-treatments)
    
    The files in `no_reoptimization_parallel_2_chifflot-3_s10.0_q220_t3` are generated for:
    
    - **average_by_query_type.csv**: Average execution time and monetary cost per query type;
    - **performances.json**: Contain the following information:
        - Average execution time for a query;
        - Average monetary cost for a query;
        - Average standard deviation for the execution time;
        - Average standard deviation for the monetary cost;
        - Average execution time for a trial (to execution the set of queries one time);
        - Number of connections.
    - **sla_violation.json**: SLA violation rate and the number of violation.
2. Graphs and table
    
    The graphs and the table are structured as follow:
    
    ```bash
    output/results/<type>_<title>_over_connections_s<database_size>_q<number_of_queries>_t<number_of_trials>.<extension>
    ```
    
    There are three graphs and one table:
    
    - **graph_execution_time**: Graph showing average execution time per query type for all algorithms and number of connections;
    - **graph_monetary_cost**: Graph showing average monetary cost per query type for all algorithms and number of connections;
    - **graph_sla_violation**: Graph showing SLA violation rate for all algorithms and number of connections;
    - **table_performances_results**: Table that resume the performances (execution time, monetary cost, sla violation and execution time for the entire query set) for all algorithms and number of connections;
        
        The first column contains the name of the algorithm, with the number of connections in brackets.
        
        Columns 2 to 5 are structured as follows: Average values +- Standard deviation (ranking)
        

### Script graphs.py

This Python script is used to generate graphs to compare algorithms.

#### Usage

```bash
graphs.py -u <g5k_user> -p <g5k_pkey_path> -s <site> [-a <algorithms>] [-S <scale>] [-q <nb_queries>] [-t <nb_trials>] [-o <output>] [-l] [-T <tests_type>] [-n <nb_connections>] [-v]
```

#### Arguments

- `-u <g5k_user>`, `--g5k-user <g5k_user>`
    
    The Grid'5000 username.
    
- `-p <g5k_pkey_path>`, `--g5k-pkey-path <g5k_pkey_path>`
    
    The path to the Grid'5000 private key.
    
    Default: ~/.ssh/grid5000_key
    
- `-s <site>`, `--site <site>`
    
    The Grid'5000 site.
    
- `-a <algorithms>`, `--algorithms <algorithms>`
    
    List of algorithms to use for the graphs (e.g. no_reoptimization, querysplit).
    
    Default: no_reoptimization
    
- `-S <scale>`, `--scale <scale>`
    
    The scale factor to use.
    
    Default: 1.0
    
- `-q <nb_queries>`, `--nb-queries <nb_queries>`
    
    The number of queries to generate (e.g. 22 queries is one query per type).
    
    Default: 22
    
- `-t <nb_trials>`, `--nb-trials <nb_trials>`
    
    Number of trials.
    
    Default: 1
    
- `-o <output>`, `--output <output>`
    
    The path to the output directory in local.
    
    Default: output/results
    
- `-l`, `--local`
    
    If the results are local or not.
    
- `-T <tests_type>`, `--tests-type <tests_type>`
    
    The type(s) of tests to select for graphs (e.g. serial or parallel).
    
    Default: serial
    
- `-n <nb_connections>`, `--nb-connections <nb_connections>`
    
    The number of connections used for the tests.
    
    Default: 1
    
- `-v`, `--verbose`
    
    Increase output verbosity.
    

#### Example

```bash
python3 graphs.py -a no_reoptimization,querysplit -S 1.0 -q 22 -t 3 -o output/results -l -T serial,parallel -n 1 -v
```

#### Output

The results will be presented as follows:

```bash
output/results
├── graph_execution_time_s0.1_q22_t3.png
├── graph_monetary_cost_s0.1_q22_t3.png
├── graph_sla_violation_s0.1_q22_t3.png
├── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
│   ├── average_by_query_type.csv
│   ├── execution_times.json
│   ├── performances.json
│   ├── sla_violation.json
│   ├── trial_1
│   ├── trial_2
│   └── trial_3
└── table_performances_results_s0.1_q22_t3.png
```

1. Generated results (post-treatments)
    
    The files in `no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3` are generated for:
    
    - **average_by_query_type.csv**: Average execution time and monetary cost per query type;
    - **performances.json**: Contain the following information:
        - Average execution time for a query;
        - Average monetary cost for a query;
        - Average standard deviation for the execution time;
        - Average standard deviation for the monetary cost;
        - Average execution time for a trial (to execution the set of queries one time);
        - Number of connections.
    - **sla_violation.json**: SLA violation rate and the number of violation.
2. Graphs and table
    
    The graphs and the table are structured as follow:
    
    ```bash
    output/results/<type>_<title>_s<database_size>_q<number_of_queries>_t<number_of_trials>.<extension>
    ```
    
    There are three graphs and one table:
    
    - **graph_execution_time**: Graph showing average execution time per query type for all algorithms;
    - **graph_monetary_cost**: Graph showing average monetary cost per query type for all algorithms;
    - **graph_sla_violation**: Graph showing SLA violation rate for all algorithms;
    - **table_performances_results**: Table that resume the performances (execution time, monetary cost, sla violation and execution time for the entire query set) for all algorithms;
        
        The first column contains the name of the algorithm, with the test type in brackets (serial or parallel).
        
        Columns 2 to 5 are structured as follows: Average values +- Standard deviation (ranking)
        

### Script run.py

This Python script is used to execute TPC-H Benchmark queries on a specific PostgreSQL algorithm.

#### Usage

```bash
run.py [-o <output>] [-a <algorithm>] [-s <scale>] [-q <nb_queries>] [-t <nb_trials>] [-T <tests_type>] [-c <nb_connections>] [-v]
```

#### Arguments

- `-o <output>`, `--output <output>`
    
    The path to the output directory.
    
    Default: ./output
    
- `-a <algorithm>`, `--algorithm <algorithm>`
    
    The algorithm to use.
    
    Default: no_reoptimization
    
- `-s <scale>`, `--scale <scale>`
    
    The scale factor to use.
    
    Default: 1
    
- `-q <nb_queries>`, `--nb-queries <nb_queries>`
    
    The number of queries to generate (e.g. 22 queries is one query per type).
    
    Default: 22
    
- `-t <nb_trials>`, `--nb-trials <nb_trials>`
    
    Number of trials.
    
    Default: 1
    
- `-T <tests_type>`, `--tests-type <tests_type>`
    
    The type of tests (queries execution) to run (e.g. serial or parallel).
    
    Default: serial
    
- `-c <nb_connections>`, `--nb-connections <nb_connections>`
    
    The number of connections to use for parallel execution tests.
    
    Default: 2
    
- `-v`, `--verbose`
    
    Increase output verbosity.
    

#### Example

```bash
python3 run.py -o ./output -a querysplit -s 0.1 -q 22 -t 3
```

#### Output

The results are structured as follow:

```bash
<output>/results
└── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
    ├── execution_times.json # Contains the execution time of each trials (time between the start and the end of the trial)
    ├── trial_1 # Contains results of the first trial
    │   └── output_execution_1.csv # Contains execution time and monetary cost of each query of the query set
    ├── trial_2
    └── trial_3
```

The results folder format is the following:

```bash
<output>/results/<algorithm>_<tests_type>_<nb_connections>_<hostname>_s<scale>_q<nb_queries>_t<nb_trials>
```

### Script setup.py

This Python script is used to setup the test environment with a specific PostgreSQL algorithm.

Setting up the environment involves the following steps:

- Checking script parameters ;
- Installation of prerequisites (bash script `libs/scripts/prerequisites.sh`) ;
- Installation of the chosen PostgreSQL version;
- Generate data with the TPC-H Benchmark tool (if not already available);
- Create database and populate with generated data.

#### Usage

```bash
setup.py [-o <output>] [-p <pg_install_path>] [-d <disk>] [-a <algorithm>] [-s <scale>] [-q <nb_queries>] [-v]
```

#### Arguments

- `-o <output>`, `--output <output>`
    
    The path to the output directory.
    
    Default: ./output
    
- `-p <pg_install_path>`, `--pg-install-path <pg_install_path>`
    
    The path to the PostgreSQL installation directory.
    
    Default: /usr/local/pgsql
    
- `-d <disk>`, `--disk <disk>`
    
    The disk to use (e.g. disk1).
    
    Default: disk1
    
- `-a <algorithm>`, `--algorithm <algorithm>`
    
    The algorithm to use.
    
    Default: no_reoptimization
    
- `-s <scale>`, `--scale <scale>`
    
    The scale factor to use.
    
    Default: 1
    
- `-q <nb_queries>`, `--nb-queries <nb_queries>`
    
    The number of queries to generate (e.g. 22 queries is one query per type).
    
    Default: 22
    
- `-v`, `--verbose`
    
    Increase output verbosity.
    

#### Example

```bash
python3 setup.py -o ./output -a querysplit -s 0.1 -q 22
```

#### Output

PostgreSQL installation will create the folder provided with the `pg_install_path` argument and install the files required for its operation.
For TPC-H data generation, this works in the same way as for the `generate_data.py` script.

## Development and maintenance

As all the files and scripts in this project are documented, the aim of this section is to explain in general terms how to update/develop the project.

### Implement a new algorithm

To implement a new algorithm, you need to create a new folder in the `algorithms/` folder. This folder must contain the following files/folders:

- `postgresql-12.3`: PostgreSQL source code (version 12.3) with the implemented algorithm;
- `initiate.py`: Python script to enable the algorithm in PostgreSQL (if needed else, the file can be empty).

### Add a new tests type

To add a new tests type, you need to create a new function in the file `libs/tpch/run_queries.py` and process the following steps:

- In the file `graphs.py`, add the new tests type in the **TESTS_TYPE_REF** variable;
- Update the documentation of all the files linked to the tests (e.g. make a research on the string *tests_type* in the project).

### Change the cloud Platform to execute the tests

Actually, the project is configured to execute the tests on Grid'5000. To change the cloud Platform, you need to change the following files:

- `g5k_execution.py`: Add another file to execute the tests on the cloud Platform or update the existing file;
- `graphs.py`: Edit the step to retrieve the results from the server;
- `libs/g5k`: Add another library to execute the tests on the cloud Platform.

### Add or edit the graphs

To add or edit the graphs, you need to edit the libraries `libs/graphs` and `libs/results` and update the `./graphs.py` script to implement the new graphs.

## Authors and acknowledgments

### Supervisors (Researchers)

- **Le gruenwald**
- **Jorge Bernardino**
- **Laurent D'Orazio**

### Students (Internship and Master's thesis)

- **Chenxiao Wang**
- **Léo Salaun**
- **Alino Tavares**
- **Steven Gourves**
