"""Generate graphs from the results experiments.

This Python script is used to generate graphs to compare algorithms.

Usage:
    graphs.py -u <g5k_user> -p <g5k_pkey_path> -s <site> [-a <algorithms>] [-S <scale>] [-q <nb_queries>] [-t <nb_trials>] [-o <output>] [-l] [-T <tests_type>] [-n <nb_connections>] [-v]

Arguments:
    -u <g5k_user>, --g5k-user <g5k_user>
        The Grid'5000 username.

    -p <g5k_pkey_path>, --g5k-pkey-path <g5k_pkey_path>
        The path to the Grid'5000 private key.
        Default: ~/.ssh/grid5000_key

    -s <site>, --site <site>
        The Grid'5000 site.

    -a <algorithms>, --algorithms <algorithms>
        List of algorithms to use for the graphs (e.g. no_reoptimization, querysplit).
        Default: no_reoptimization

    -S <scale>, --scale <scale>
        The scale factor to use.
        Default: 1.0

    -q <nb_queries>, --nb-queries <nb_queries>
        The number of queries to generate (e.g. 22 queries is one query per type).
        Default: 22

    -t <nb_trials>, --nb-trials <nb_trials>
        Number of trials.
        Default: 1

    -o <output>, --output <output>
        The path to the output directory in local.
        Default: output/results

    -l, --local
        If the results are local or not.

    -T <tests_type>, --tests-type <tests_type>
        The type(s) of tests to select for graphs (e.g. serial or parallel).
        Default: serial

    -n <nb_connections>, --nb-connections <nb_connections>
        The number of connections used for the tests.
        Default: 1

    -v, --verbose
        Increase output verbosity.

Example:
    python3 graphs.py -a no_reoptimization,querysplit -S 1.0 -q 22 -t 3 -o output/results -l -T serial,parallel -n 1 -v

Output:
    The results will be presented as follows:

    ```bash
    output/results
    ├── graph_execution_time_s0.1_q22_t3.png
    ├── graph_monetary_cost_s0.1_q22_t3.png
    ├── graph_sla_violation_s0.1_q22_t3.png
    ├── no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3
    │   ├── average_by_query_type.csv
    │   ├── execution_times.json
    │   ├── performances.json
    │   ├── sla_violation.json
    │   ├── trial_1
    │   ├── trial_2
    │   └── trial_3
    └── table_performances_results_s0.1_q22_t3.png
    ```

    1. Generated results (post-treatments)
        
        The files in `no_reoptimization_serial_2_chifflot-4_s0.1_q22_t3` are generated for:
        
        - **average_by_query_type.csv**: Average execution time and monetary cost per query type;
        - **performances.json**: Contain the following information:
            - Average execution time for a query;
            - Average monetary cost for a query;
            - Average standard deviation for the execution time;
            - Average standard deviation for the monetary cost;
            - Average execution time for a trial (to execution the set of queries one time);
            - Number of connections.
        - **sla_violation.json**: SLA violation rate and the number of violation.
    2. Graphs and table
        
        The graphs and the table are structured as follow:
        
        ```bash
        output/results/<type>_<title>_s<database_size>_q<number_of_queries>_t<number_of_trials>.<extension>
        ```
        
        There are three graphs and one table:
        
        - **graph_execution_time**: Graph showing average execution time per query type for all algorithms;
        - **graph_monetary_cost**: Graph showing average monetary cost per query type for all algorithms;
        - **graph_sla_violation**: Graph showing SLA violation rate for all algorithms;
        - **table_performances_results**: Table that resume the performances (execution time, monetary cost, sla violation and execution time for the entire query set) for all algorithms;
            
            The first column contains the name of the algorithm, with the test type in brackets (serial or parallel).
            Columns 2 to 5 are structured as follows: Average values +- Standard deviation (ranking)
"""

import glob
import argparse
import logging

from libs.graphs import GraphExecutionTime, GraphMonetaryCost, GraphSLAViolation, GraphPerformances
from libs.results import ResAverage, ResSLAViolation, ResPerformances
from libs.g5k import G5kNode

# Constants
ALGORITHMS_REF = [
    {"ref": "no_reoptimization", "name": "NoReOpt"},
    {"ref": "querysplit", "name": "Query Split"}]
TESTS_TYPE_REF = [
    {"ref": "serial", "name": "Serial"},
    {"ref": "parallel", "name": "Parallel"}]


# Use parse_args() to get the arguments
parser = argparse.ArgumentParser(
    prog="graphs.py",
    description="Generate graphs from the results of the experiments.")

parser.add_argument(
    "-u", "--g5k-user", type=str, required=True,
    help="The Grid'5000 username.")
parser.add_argument(
    "-p", "--g5k-pkey-path", type=str, default="~/.ssh/grid5000_key",
    help="The path to the Grid'5000 private key. Defaults to ~/.ssh/grid5000_key.")
parser.add_argument(
    "-s", "--site", type=str, required=True,
    help="The site to use (e.g. lille).")
parser.add_argument(
    "-a", "--algorithms", type=str, default="no_reoptimization",
    help="List of algorithms to use for the graphs (e.g. no_reoptimization, querysplit). Defaults to no_reoptimization.")
parser.add_argument(
    "-S", "--scale", type=float, default=1.0,
    help="The scale factor to use. Defaults to 1.0.")
parser.add_argument(
    "-q", "--nb-queries", type=int, default=22,
    help="The number of queries to generate (e.g. 22 queries is one query per type). Defaults to 22.")
parser.add_argument(
    "-t", "--nb-trials", type=int, default=1,
    help="Number of trials. Defaults to 1.")
parser.add_argument(
    "-o", "--output", type=str, default="output/results",
    help="The path to the output directory in local. Defaults to output/results.")
parser.add_argument(
    "-l", "--local", action="store_true",
    help="If the results are local or not.")
parser.add_argument(
    "-T", "--tests-type", type=str, default="serial",
    help="The type(s) of tests to select for graphs (e.g. serial or parallel). Defaults to serial.")
parser.add_argument(
    "-n", "--nb-connections", type=int, default=1,
    help="The number of connections used for the tests. Defaults to 1.")
parser.add_argument(
    "-v", "--verbose", action="store_true",
    help="Increase output verbosity.")

args = parser.parse_args()

# Configure the logger
level = logging.DEBUG if args.verbose else logging.INFO
logging.basicConfig(
    # filename=f"graphs.log", # For debugging only
    level=level,
    format="%(asctime)s :: %(levelname)s :: %(message)s")

# Variables
algorithms = args.algorithms.split(",")
selected_algorithms = [a for a in ALGORITHMS_REF if a["ref"] in algorithms]
tests_type = args.tests_type.split(",")
selected_tests_type = [t for t in TESTS_TYPE_REF if t["ref"] in tests_type]

# 0. Get results from the Grid'5000 server
if not args.local:
    logging.info("GRAPH: 0. Get results from the Grid'5000 server")
    g5k = G5kNode(
        g5k_user=args.g5k_user, g5k_pkey=args.g5k_pkey_path, site=args.site, node="")
    g5k.get_results("results/", args.output)

# 1. Generate data (averages, SLA violation and Standard deviation) from results
logging.info(
    "GRAPH: 1. Generate data (averages, SLA violation and Standard deviation) from results")
for algorithm in selected_algorithms:
    for type in selected_tests_type:
        # Variables
        results_folder_path_pattern = f"{args.output}/{algorithm['ref']}_{type['ref']}_*_s{args.scale}_q{args.nb_queries}_t{args.nb_trials}"
        # Get the first result path folder
        results_path = next(iter(
            [f for f in glob.glob(results_folder_path_pattern)]), None)

        # Check if path exist
        if results_path is None:
            logging.error(
                f"No results found for {algorithm['ref']}_{type['ref']}, path {results_folder_path_pattern}.")
            raise Exception(
                f"No results found for {algorithm['ref']}_{type['ref']}, path {results_folder_path_pattern}.")

        # 1.1 Averages (RESULTS_PATH/average_by_query_type.csv)
        logging.info(
            "GRAPH: 1.1 Averages (RESULTS_PATH/average_by_query_type.csv)")
        ResAverage.average_by_query_type(results_path)

        # 1.2 SLA violation (RESULTS_PATH/sla_violation.json)
        logging.info(
            "GRAPH: 1.2 SLA violation (RESULTS_PATH/sla_violation.json)")
        ResSLAViolation.sla_violation(
            results_path, args.scale, args.nb_queries)

        # 1.3 Standard deviation (RESULTS_PATH/standard_deviation.csv)
        logging.info(
            "GRAPH: 1.3 Standard deviation (RESULTS_PATH/standard_deviation.csv)")
        ResPerformances.performances(results_path, args.nb_connections)

# 2. Generate Graph for execution time
logging.info("GRAPH: 2. Generate Graph for execution time")
GraphExecutionTime.graph_execution_time(
    f"{args.output}", selected_algorithms, selected_tests_type,
    args.scale, args.nb_queries, args.nb_trials, args.nb_connections)

# 3. Generate graph for monetary cost
logging.info("GRAPH: 3. Generate graph for monetary cost")
GraphMonetaryCost.graph_monetary_cost(
    f"{args.output}", selected_algorithms, selected_tests_type,
    args.scale, args.nb_queries, args.nb_trials, args.nb_connections)

# 4. Generate graph for SLA violation
logging.info("GRAPH: 4. Generate graph for SLA violation")
GraphSLAViolation.sla_violation(
    f"{args.output}", selected_algorithms, selected_tests_type,
    args.scale, args.nb_queries, args.nb_trials, args.nb_connections)

# 5. Generate graph for the standard deviation
logging.info("GRAPH: 5. Generate graph for the standard deviation")
GraphPerformances.perf_results(
    f"{args.output}", selected_algorithms, selected_tests_type,
    args.scale, args.nb_queries, args.nb_trials)

logging.info("GRAPH: Done")
